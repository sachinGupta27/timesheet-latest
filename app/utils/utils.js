import { Dimensions,AsyncStorage } from 'react-native';
const STANDARD_WIDTH = 375;
const CURRENT_WIDTH = Dimensions.get('window').width;
const K = CURRENT_WIDTH / STANDARD_WIDTH;

const USE_FOR_BIGGER_SIZE = true;

global.flag = false;
global.selectedProject = '';
global.play=false
global.backgroundSeconds=0

export const height = Dimensions.get('window').height;


export const width = Dimensions.get('window').width;


export const fontSizes = {
    font10: getFontSize(10),
    font12: getFontSize(12),
    font14: getFontSize(14),
    font18: getFontSize(18),
    font16: getFontSize(16),
    font20: getFontSize(20)
}

export const colorArray =
    ['#2A56C6', '#516EB7', '#6893FF', '#77B5FE', "#002366",
        "#4169E1", "#0038A8", "#4997D0",
        "#436B95", "#0BB5FF", "#5D8AA8", "#000080", "#F9EA0", "#98F5FF", "#8EE5EE"
        , "#1974D2", '#2A56C6', '#516EB7', '#6893FF', '#77B5FE', "#002366",
        "#4169E1", "#0038A8", "#4997D0",
        "#436B95", "#0BB5FF", "#5D8AA8", "#000080", "#F9EA0", "#98F5FF", "#8EE5EE"
        , "#1974D2",'#2A56C6', '#516EB7', '#6893FF', '#77B5FE', "#002366",
        "#4169E1", "#0038A8", "#4997D0",
        "#436B95", "#0BB5FF", "#5D8AA8", "#000080", "#F9EA0", "#98F5FF", "#8EE5EE"
        , "#1974D2",'#2A56C6', '#516EB7', '#6893FF', '#77B5FE', "#002366",
        "#4169E1", "#0038A8", "#4997D0",
        "#436B95", "#0BB5FF", "#5D8AA8", "#000080", "#F9EA0", "#98F5FF", "#8EE5EE"
        , "#1974D2",'#2A56C6', '#516EB7', '#6893FF', '#77B5FE', "#002366",
        "#4169E1", "#0038A8", "#4997D0",
        "#436B95", "#0BB5FF", "#5D8AA8", "#000080", "#F9EA0", "#98F5FF", "#8EE5EE"
        , "#1974D2",
    ]



//const { width } = Dimensions.get('window');


export function dynamicSize(size) {
    return K * size;
}

function getFontSize(size) {
    if (USE_FOR_BIGGER_SIZE || CURRENT_WIDTH < STANDARD_WIDTH) {
        const newSize = K * size
        return newSize;
    }
    return size;
}

export function getLog(log) {
    console.log(log)
}