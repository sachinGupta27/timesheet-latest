/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, StatusBar, Keyboard, TouchableWithoutFeedback,
    Text, SafeAreaView, TouchableOpacity,Animated,Easing,
    View, Image, TextInput, ScrollView, AsyncStorage
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { getLog } from '../utils/utils';



export default class splash extends Component {

    constructor () {
        super()
        this.animatedValue = new Animated.Value(0)
      }
      componentDidMount () {
        this.animate()
      }
      animate () {
        this.animatedValue.setValue(0)
        Animated.timing(
          this.animatedValue,
          {
            toValue: 1,
            duration: 4000,
            easing: Easing.linear
          }
        ).start(() => this.animate())
      }

    componentWillMount() {
        var userToken = ''
        AsyncStorage.getItem('userToken').then((token) => {

            userToken = token == null ? '' : token
        })
        setTimeout(() => {
            if (userToken.length > 0) {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Drawer' })],
                });
                this.props.navigation.dispatch(resetAction);
                AsyncStorage.getItem('LoginresponseData').then((data) => {
                    getLog("userToken===" + userToken)
                    getLog("LoginresponseData===" + JSON.stringify(data))
                })
            } else {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Login' })],
                });
                this.props.navigation.dispatch(resetAction);
            }
        }, 2000)
    }
    render() {
        const opacity = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 1, 0]
          })
        return (
            <SafeAreaView
                style={styles.safeArea}>
                <View style={styles.container}>
                    <Animated.Image style={{opacity}} source={require('../images/loginSignup/Simplitime.png')} />
                </View>



            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2A56C6',
    },
    iOSTopBar: {
        backgroundColor: '#21459E',
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },

});
