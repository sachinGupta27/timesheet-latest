



/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, SafeAreaView,
    Text, ScrollView, FlatList, AsyncStorage, PanResponder,
    View, Image, TouchableOpacity, TouchableWithoutFeedback
} from 'react-native';
import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import PickerDropdownModal from '../components/pickerDropdownModal'
import { height, width, fontSizes, colorArray, getLog } from '../utils/utils'
import webservice from '../components/webService'
import Spinner from '../components/spinner'
import { dateConverter, year_Array, week_Date_Array, dateConverterUseInWeekTab, hourMinuteConverter } from '../components/dateConverter'
const monthArr = ["January", "Feburary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
const data = [
    1, 2, 3, 4, 5, 6, 7, 8
]
export default class projectBreakdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [], weekArray: [], ActivityList: [], returnCallBack: { value: false, callBack: () => { } },
            pickerDropdownModalVisible: false,
            pickerDropdownModalTitleText: "Week",
            week: "Week",
            week_Date_Array_Middle_Index: this.props.navigation.state.params.filterData.week_Date_Array_Middle_Index,
            weekArray: [],
            spinnerVisible: false,
            userToken: '',
            totalActivitiesHours: 0,
            dataLoading: false,
            filterDataPassedOnProjectDevelopment: '',


            arrayPassedToTabScrollView: ''

        }
    }
    componentWillMount() {
        AsyncStorage.getItem('userToken').then((token) => {
            this.setState({ userToken: token })
            //this.makeInitial_Week_Month_Year_Tab(token, this.props.navigation.state.params.filterData)
            //this.makeWeekDateTabArray(token)

        })

       // alert(JSON.stringify(this.props.navigation.state.params))
        this.setState({ data: this.props.navigation.state.params.data })
        //alert(JSON.stringify(this.props.navigation.state.params.filterData))
        //this.makeInitial_Week_Month_Year_Tab(this.props.navigation.state.params.filterData)
        // setTimeout(() => {
        //     alert(JSON.stringify(this.state.data))
        // }, 500)

        // var activityArr = [
        //     { "activityName": "Development", "Billable": true, "time": 65 },
        //     { "activityName": "Meeting", "Billable": false, "time": 47 },
        //     { "activityName": "Development", "Billable": true, "time": 62 },
        //     { "activityName": "Development", "Billable": true, "time": 94 },
        //     { "activityName": "Development", "Billable": true, "time": 120 },
        //     { "activityName": "Meeting", "Billable": false, "time": 75 },
        // ]
        // this.setState({ ActivityList: activityArr })
        this.setState({ returnCallBack: { value: false, callBack: this.callBackCalledOnActivityAdd } })


        this.wrapperPanResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderEnd: (e, { vx, dx }) => {

                console.log("end" + dx)
                if (dx < 0) {
                    this.setState({ globalLeft: "right" })
                    if (this.state.week == 'Month') {
                        if (this.state.week_Date_Array_Middle_Index < 10) {
                            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
                            setTimeout(() => {
                                this.scrollToPosition(this.state.userToken)
                            }, 300)
                        }
                    } else {
                        if (this.state.week_Date_Array_Middle_Index < 49) {
                            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
                            setTimeout(() => {
                                this.scrollToPosition(this.state.userToken)
                            }, 300)
                        }
                    }
                    console.log("right")
                } else {
                    this.setState({ globalLeft: "left" })
                    if (this.state.week == 'Month') {



                        if (this.state.week_Date_Array_Middle_Index > 1) {
                            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
                            setTimeout(() => {
                                this.scrollToPosition(this.state.userToken)
                            }, 300)

                        } else {
                            //alert(this.state.week_Date_Array_Middle_Index)
                            if (this.state.week_Date_Array_Middle_Index > -1) {
                                this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
                                this.scroller.scrollTo({ x: 0, y: 0 })
                                setTimeout(() => {
                                    this.hitGetProjectActivityListAPI(this.state.userToken, "month", this.state.week_Date_Array_Middle_Index + 2)
                                })
                            }

                        }
                    } else {
                        if (this.state.week_Date_Array_Middle_Index > 1) {
                            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
                            setTimeout(() => {
                                this.scrollToPosition(this.state.userToken)
                            }, 300)
                        }
                    }

                    console.log("left")
                }
                return true;
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
                return true;
            },
        });

    }
    componentDidMount() {
        //this.refs.scrollView.scrollTo({ x: (((25) * width / 3)), y: 0, animated: true });
        //this.flatListRef.scrollToIndex({animated: true, index: 26});
        if (this.props.navigation.state.params.filterData.action == "week") {
            this.setState({ week: "Week", arrayPassedToTabScrollView: week_Date_Array })
        } else if (this.props.navigation.state.params.filterData.action == "month") {
            this.setState({ week: "Month", arrayPassedToTabScrollView: monthArr })
        } else {
            this.setState({ week: "Year", arrayPassedToTabScrollView: year_Array })
        }
        AsyncStorage.getItem('userToken').then((token) => {
            this.scrollToPosition(token)
        })

    }
    scrollToPosition(token) {
        if (this.state.week == 'Week') {
            this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width / 3), y: 0 })

            this.hitGetProjectActivityListAPI(token, "week", new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 1].startDate).getTime(), new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 1].endDate).getTime())

        } else if (this.state.week == 'Month') {

            this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width / 3), y: 0 })

            this.hitGetProjectActivityListAPI(token, "month", this.state.week_Date_Array_Middle_Index + 2)

        } else {
            this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width / 3), y: 0 })
            this.hitGetProjectActivityListAPI(this.state.userToken, "year", year_Array[this.state.week_Date_Array_Middle_Index + 1])

        }


    }

    tabs() {
        var arr = []
        for (var i = 0; i < this.state.arrayPassedToTabScrollView.length; i++) {
            arr.push(this.makeColumn(this.state.arrayPassedToTabScrollView[i], i))
        }
        return (
            arr
        )

    }
    makeColumn(item, i) {
        return (
            <View
                style={[styles.tabStyle, { width: width / 3 }]} >
                <Text style={[styles.tabText, { color: (this.state.week_Date_Array_Middle_Index + 1) != i ? "#2A56C6" : "#000", fontWeight: (this.state.week_Date_Array_Middle_Index + 1) != i ? "normal" : "bold" }]}>
                    {this.state.week == "Week" ?
                        dateConverterUseInWeekTab(new Date(item.startDate)) + "-" + dateConverterUseInWeekTab(new Date(item.endDate))
                        :
                        item
                    }
                </Text>
            </View>
        )
    }
    makeInitial_Week_Month_Year_Tab(token, filter) {
        if (filter.action == "week") {

            this.setState({ week_Date_Array_Middle_Index: filter.week_Date_Array_Middle_Index, week: "Week" })
            setTimeout(() => {
                this.makeWeekDateTabArray(token)
            }, 300)

        } else if (filter.action == "month") {

            //var month = new Date().getMonth()
            month = filter.week_Date_Array_Middle_Index
            this.setState({ week_Date_Array_Middle_Index: filter.week_Date_Array_Middle_Index, week: "Month" })
            if (month > 0 && month < 11) {
                setTimeout(() => {
                    this.setState({ weekArray: [monthArr[month - 1], monthArr[month], monthArr[month + 1]] })
                }, 300)
            } else {
                if (month == 0) {
                    setTimeout(() => {
                        this.setState({ weekArray: [monthArr[month], monthArr[month + 1], monthArr[month + 2]], week_Date_Array_Middle_Index: 1 })
                    }, 300)
                } else {
                    setTimeout(() => {
                        this.setState({ weekArray: [monthArr[month - 2], monthArr[month - 1], monthArr[month]], week_Date_Array_Middle_Index: 10 })
                    }, 300)
                }

            }
            setTimeout(() => {

                this.hitGetProjectActivityListAPI(token, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]))
            }, 300)


        } else {
            var year = filter.week_Date_Array_Middle_Index
            this.setState({ week_Date_Array_Middle_Index: filter.week_Date_Array_Middle_Index, week: "Year" })
            setTimeout(() => {
                this.hitGetProjectActivityListAPI(token, "year", this.state.week_Date_Array_Middle_Index)
                this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
            }, 300)
        }
    }
    onWeekTabPressLeft() {
        if (this.state.week == "Week") {
            if (this.state.week_Date_Array_Middle_Index > 1) {

                this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
                setTimeout(() => {
                    this.makeWeekDateTabArray(this.state.userToken)
                }, 300)
            }
        } else if (this.state.week == "Month") {
            if (this.state.week_Date_Array_Middle_Index > 1) {
                this.hitGetProjectActivityListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]) - 1)
                this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
                setTimeout(() => {
                    this.setState({ weekArray: [monthArr[this.state.week_Date_Array_Middle_Index - 1], monthArr[this.state.week_Date_Array_Middle_Index], monthArr[this.state.week_Date_Array_Middle_Index + 1]] })
                }, 300)
            }
        } else {
            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
            setTimeout(() => {
                this.hitGetProjectActivityListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
                this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
            }, 300)
        }



    }
    onWeekTabPressRight() {
        if (this.state.week == "Week") {
            if (this.state.week_Date_Array_Middle_Index < 51) {
                this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
                setTimeout(() => {
                    this.makeWeekDateTabArray(this.state.userToken)
                }, 300)
            }
        } else if (this.state.week == "Month") {
            if (this.state.week_Date_Array_Middle_Index < 10) {
                this.hitGetProjectActivityListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]) + 1)
                this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
                setTimeout(() => {
                    this.setState({ weekArray: [monthArr[this.state.week_Date_Array_Middle_Index - 1], monthArr[this.state.week_Date_Array_Middle_Index], monthArr[this.state.week_Date_Array_Middle_Index + 1]] })
                }, 300)
            }
        } else {
            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
            setTimeout(() => {
                this.hitGetProjectActivityListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
                this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
            }, 300)
        }
    }
    makeWeekDateTabArray(token) {
        var index = this.state.week_Date_Array_Middle_Index
        var weekArray = []
        weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index - 1].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index - 1].endDate)))
        weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index].endDate)))
        weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index + 1].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index + 1].endDate)))
        this.setState({ weekArray: weekArray })

        this.hitGetProjectActivityListAPI(token, "week", new Date(week_Date_Array[index].startDate).getTime(), new Date(week_Date_Array[index].endDate).getTime())
    }
    hitGetProjectActivityListAPI(token, action, startDate, endDate) {
        let variables = new FormData();


        variables.append("action", action)
        variables.append("startDate", startDate)
        variables.append("projectId", this.state.data.projectId)

        if (action == 'week') {
            variables.append("endDate", endDate)
        }



        //this.setState({ spinnerVisible: true })
        this.setState({ dataLoading: true })
        //dataLoading
        getLog("startDate:===" + new Date(startDate) + "====enddate:=====" + new Date(endDate))
        return webservice(variables, "timesheet/getactiviteslist", 'Post', token)
            .then(response => {
                this.setState({ spinnerVisible: false, dataLoading: false })
                if (response != "error") {
                    if (response.timesheetActivityList.length != 0) {

                        var arr = []

                        for (var i = 0; i < response.timesheetActivityList.length; i++) {
                            arr.push(
                                {
                                    "userId": response.timesheetActivityList[i].userId,
                                    "projectId": response.timesheetActivityList[i].projectId,
                                    'activityId': response.timesheetActivityList[i].activityId,
                                    'activityName': response.timesheetActivityList[i].activityName,
                                    'Billable': response.timesheetActivityList[i].billable,
                                    'userId': response.timesheetActivityList[i].userId,
                                    "time": response.timesheetActivityList[i].totalHours,

                                })
                        }

                        this.totalHoursMinutesOfActivities(response.timesheetActivityList)

                        this.setState({ ActivityList: arr })
                        //this is done to set the filter data that is passed to project development screen
                        

                    } else {
                        this.setState({ ActivityList: [] })
                    }
                    //this.totalNumberOfDaysLeave()
                    this.totalHoursMinutesOfActivities(response.timesheetActivityList)
                    getLog("Timesheet project list List of time sheet page=====++++" + JSON.stringify(response))

                    if (action == 'week') {
                        this.setState({
                            filterDataPassedOnProjectDevelopment: {
                                "startDate": startDate,
                                "action": action,
                                "endDate": endDate
                            }
                        })


                    } else {
                        this.setState({
                            filterDataPassedOnProjectDevelopment: {
                                "startDate": startDate,
                                "action": action,
                                "endDate": ''
                            }
                        })

                    }

                }

            })
    }
    totalHoursMinutesOfActivities(response) {

        var minutes = 0
        this.setState({ totalActivitiesHours: 0 })

        for (var i = 0; i < response.length; i++) {
            //getLog("==========++++++++++++=============")
            minutes = minutes + Number(response[i].totalHours)

        }


        this.setState({ totalActivitiesHours: minutes })

    }
    pickerDropdownModalRowSelected(item) {

        this.setState({ week: item })

        // if (item == "Week") {
        //     this.setState({ week_Date_Array_Middle_Index: 26 })
        //     setTimeout(() => {
        //         this.makeWeekDateTabArray(this.state.userToken)
        //     }, 300)

        // } else if (item == "Month") {

        //     var month = new Date().getMonth()
        //     this.setState({ week_Date_Array_Middle_Index: month })
        //     if (month > 0 && month < 11) {
        //         setTimeout(() => {
        //             this.setState({ weekArray: [monthArr[month - 1], monthArr[month], monthArr[month + 1]] })
        //         }, 300)
        //     } else {
        //         if (month == 0) {
        //             setTimeout(() => {
        //                 this.setState({ weekArray: [monthArr[month], monthArr[month + 1], monthArr[month + 2]], week_Date_Array_Middle_Index: 1 })
        //             }, 300)
        //         } else {
        //             setTimeout(() => {
        //                 this.setState({ weekArray: [monthArr[month - 2], monthArr[month - 1], monthArr[month]], week_Date_Array_Middle_Index: 10 })
        //             }, 300)
        //         }

        //     }
        //     setTimeout(() => {

        //         this.hitGetProjectActivityListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]))
        //     }, 300)


        // } else {
        //     var year = new Date().getFullYear()
        //     this.setState({ week_Date_Array_Middle_Index: year })
        //     setTimeout(() => {
        //         this.hitGetProjectActivityListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
        //         this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
        //     }, 300)
        // }

        if (item == "Week") {
            this.setState({ week_Date_Array_Middle_Index: 25, arrayPassedToTabScrollView: week_Date_Array, week: "Week" })
            setTimeout(() => {
                this.scrollToPosition(this.state.userToken)
            }, 300)
        } else if (item == "Month") {
            var month = new Date().getMonth()
            this.scroller.scrollTo({ x: 0, y: 0,animated:false })
            this.setState({ week_Date_Array_Middle_Index: month - 1, arrayPassedToTabScrollView: monthArr, week: "Month" })
            setTimeout(() => {
                this.scrollToPosition(this.state.userToken)
            }, 300)
        } else {
            var year = new Date().getFullYear()
            this.setState({ week_Date_Array_Middle_Index: year_Array.indexOf(year - 1), arrayPassedToTabScrollView: year_Array, week: "Year" })
            setTimeout(() => {
                this.scrollToPosition(this.state.userToken)
            }, 300)
        }


        this.setState({ pickerDropdownModalVisible: false })
    }
    weekTabs() {
        return (
            <View style={styles.weekTabView}>
                {/* first tab */}
                <TouchableWithoutFeedback onPress={() => this.onWeekTabPressLeft()}>
                    <View
                        style={[styles.tabStyle]} >
                        <Text style={[styles.tabText, { color: "#2A56C6" }]}>
                            {this.state.weekArray[0]}
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                {/* second tab */}
                <TouchableWithoutFeedback onPress={() => this.onWeekTabPressLeft()}>
                    <View
                        style={[styles.tabStyle]}   >
                        <Text style={[styles.tabText, { color: "#000", fontWeight: "bold" }]}>
                            {this.state.weekArray[1]}
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                {/* third tab */}
                <TouchableWithoutFeedback onPress={() => this.onWeekTabPressRight()}>
                    <View onPress={() => this.onWeekTabPressRight()}
                        style={[styles.tabStyle]}   >
                        <Text style={[styles.tabText, { color: "#2A56C6" }]}>
                            {this.state.weekArray[2]}
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }
    callBackCalledOnActivityAdd = (token, data) => {
        //alert(JSON.stringify(data))
        // this.state.ActivityList.push({ "activityName": data.activity, "Billable": data.billable, "time": data.time })
        // this.setState({ ActivityList: this.state.ActivityList })
        this.hitGetProjectActivityListAPI(token, data.action, data.startDate, data.endDate)
        this.props.navigation.state.params.callbackPassedToProjectBreakdown.callBack(token, data)
        //this callback is called when we click on + floating icon and add a new activity that is added to this screen flatlist
    }


    createList(item, index) {
        return (
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate("ProjectDevelopment", { data: item, projectName: this.state.data.projectName, projectId: this.state.data.projectId, filterDataPassedOnProjectDevelopment: this.state.filterDataPassedOnProjectDevelopment })}
                key={index + "row"} >
                <View style={styles.listView}>
                    <View style={styles.listRowView}>
                        <View style={{ flex: 1, alignItems: "center", justifyContent: "center", }}>
                            <View style={{ height: 20, width: 20, borderRadius: 10, backgroundColor: colorArray[index] ? colorArray[index] : "#6893FF" }}></View>
                        </View>
                        <View style={[styles.projectColumnView,]}>
                            <Text style={{ fontSize: fontSizes.font14, }}>
                                {item.activityName}
                            </Text>
                            <Text style={{ fontSize: fontSizes.font12, color: "#BBBBBB", paddingTop: 5 }}>
                                {item.Billable}
                            </Text>
                        </View>

                        <View style={[styles.projectColumnView, { flex: 2, flexDirection: "row", alignItems: "center", justifyContent: "flex-end" }]}>

                            <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                                {hourMinuteConverter(item.time)}
                            </Text>
                            <Image source={require('../images/rightArrow.png')} />

                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
    onFloatingButtonPressed() {
        this.props.navigation.navigate("CreateTimesheet", { data: { projectName: this.state.data.projectName, projectId: this.state.data.projectId, returnCallBack: this.state.returnCallBack, filterData: this.state.filterDataPassedOnProjectDevelopment } })
    }
    onWeekSelectDropdown() {
        this.setState({ pickerDropdownModalVisible: true, PickerDropdownModalType: "Week", pickerDropdownModalListData: ["Week", "Month", "Year"] })
    }
    render() {

        return (
            <SafeAreaView style={styles.safeArea}>
                <Spinner visible={this.state.spinnerVisible} />
                <View style={styles.container}>
                    <Header
                        HeaderLeftText={this.state.data.projectName}
                        headerType='back'
                        HeaderRightText={this.state.week}

                        toggleDrawer={() => this.props.navigation.goBack()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIconOnPress={() => this.onWeekSelectDropdown()}
                    />
                    <PickerDropdownModal
                        pickerDropdownModalVisible={this.state.pickerDropdownModalVisible}
                        pickerDropdownModalClose={() => this.setState({ pickerDropdownModalVisible: false })}
                        //titleText={this.state.pickerDropdownModalTitleText}
                        titleText="Select Type"
                        pickerDropdownModalListData={this.state.pickerDropdownModalListData}
                        pickerDropdownModalRow={({ item, index }) =>
                            <TouchableWithoutFeedback onPress={() => this.pickerDropdownModalRowSelected(item)}>
                                <View style={{ padding: 15, paddingLeft: 25, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                                    <Text style={{ fontSize: fontSizes.font18, color: "#333333" }}>
                                        {item}
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                        }
                    />
                    <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />
                    <View style={styles.mainContainer}>
                        {/* {this.weekTabs()} */}
                        <View style={{ height: 35 }}>
                            <ScrollView
                                //pagingEnabled
                                style={{ height: 35, backgroundColor: "#DADADA" }}
                                showsHorizontalScrollIndicator={false}
                                onChangeVisibleColumns={(visibleRows) => alert(visibleRows)}
                                //onScroll={(event)=>this.scrollToPosition()}
                                //scrollEventThrottle={16}
                                //onScrollEndDrag={() => this.scrollToPosition()}
                                ref={(scroller) => { this.scroller = scroller }}
                                horizontal
                                {...this.wrapperPanResponder.panHandlers}
                            >
                                {this.state.week_Date_Array_Middle_Index > -3 ? this.tabs() : null}
                            </ScrollView>
                        </View>
                        <View style={styles.rowView}>
                            <Text style={styles.headerTextStyle}>Activity</Text>
                            <Text style={[styles.headerTextStyle, { fontSize: fontSizes.font18, color: "#FF9800" }]}>{hourMinuteConverter(this.state.totalActivitiesHours)}</Text>
                        </View>

                        {this.state.dataLoading ?
                            <Text style={[styles.ProjectHeaderText, { fontSize: fontSizes.font14, marginTop: 15 }]}>
                                Loading
                            </Text>
                            :
                            (!this.state.dataLoading && this.state.ActivityList.length == 0) ?
                                <Text style={[styles.ProjectHeaderText, { fontSize: fontSizes.font14, marginTop: 15 }]}>
                                    No Activity Found For This Project In This Time Period
                            </Text>
                                :
                                <FlatList
                                    data={this.state.ActivityList}
                                    renderItem={({ item, index }) => this.createList(item, index)}
                                    keyExtractor={key => key.index}
                                />
                        }


                    </View>

                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#fff'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    weekTabView: { height: 35, width: width, flexDirection: 'row', backgroundColor: "#DADADA" },
    tabStyle: { flex: 1, alignItems: "center", justifyContent: "center" },
    tabText: { fontSize: fontSizes.font12, color: "white", textAlign: 'center', },
    mainContainer: { flex: 1, borderBottomColor: "#E5E5E5", borderBottomWidth: 1 },
    ProjectHeaderText: { fontSize: fontSizes.font16, color: "#2A56C6", textAlign: 'center' },
    rowView: { flexDirection: "row", justifyContent: "space-between", paddingBottom: 10, padding: 3, borderBottomColor: "#E5E5E5", borderBottomWidth: 1, margin: 15, marginBottom: 10, alignItems: "center" },
    headerTextStyle: { fontSize: fontSizes.font16, color: "#2A56C6", textAlign: 'left' },
    listView: { borderBottomWidth: .5, marginHorizontal: 5, marginTop: 0, paddingBottom: 5, borderBottomColor: '#E5E5E5', marginRight: 10 },
    listRowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
    projectColumnView: { flex: 4, justifyContent: "center", },
});
