



/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, SafeAreaView, Keyboard, AsyncStorage,
    Text, ScrollView, FlatList, TextInput, Alert,
    View, Image, TouchableOpacity, TouchableWithoutFeedback
} from 'react-native';
//import Geolocation from 'react-native-geolocation-service';
import Header from '../components/commonHeader'
import webservice from '../components/webService'
import { dateConverterUseInHeader, hourMinuteConverter, dateConverter } from '../components/dateConverter'
import { height, width, fontSizes, getLog } from '../utils/utils'
import TimeModal from '../components/timeModal'
import DateTimePicker from 'react-native-modal-datetime-picker';
import Spinner from '../components/spinner'
import ProjectModal from '../components/projectModal'
export default class createTimesheet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '', activity: '', notes: '', timePickerType: "", startTimeMillisecond: 0, endTimeMillisecond: 0,
            startTime: 'Start Time', endTime: 'End Time', billable: false, time: 0, timePickerVisible: false,
            timeModalVisible: false, hourText: 0, minuteText: 0, billable: false, editTimesheet: false, deletedIndex: null,
            activityArr: [], activityTextFieldWrites: false, activityArrPassedToSearchFlatList: [], userToken: '',
            projectNameEditable: true, activityEditable: true, projectArr: [], projectTextFieldWrites: false, projectArrPassedToSearchFlatList: [],
            activityId: '', projectId: '', spinnerVisible: false, location: '', showLocationTextField: false,
            latitude: '', longitude: '', datePickerVisible: false, date: "Date", scrollable: true,
            filterData: '',
            //project modal states
            projectModalVisible: false,
            projectNameModalText: '',
            projectCodeModalText: '',
            projectBillingModalText: '',
            projectCurrencyModalText: '',
            projectModalDatePickerVisible: false,
            startDateToShowOnMOdal: '',
            endDateToShowOnModal: '',
            minDate: '',
            projectCurrencyModalText: 'Currency',
            projectNameMandatory: false,
            projectNameMandatoryText: '',
            datePickerType: '',
            startDate: '',
            endDate: ''
        }
    }
    componentWillMount() {
        //alert(JSON.stringify(this.props.navigation.state.params))
        // this.setState({
        //     activityArr: []
        // })
        this.props.navigation.state.params.data ?
            this.setState({
                filterData: this.props.navigation.state.params.data.filterData,
            }) : null
        this.props.navigation.state.params.data ?
            this.props.navigation.state.params.data.projectName ?
                this.setState({
                    name: this.props.navigation.state.params.data.projectName,
                    projectNameEditable: false,
                    projectId: this.props.navigation.state.params.data.projectId,
                    filterData: this.props.navigation.state.params.data.filterData,
                }) : null : null
        if (this.props.navigation.state.params.data) {
            this.props.navigation.state.params.data.otherData ?
                this.setState({
                    startTime: this.props.navigation.state.params.data.otherData.timeTo == 0 ? 'Start Time' : new Date(Number(this.props.navigation.state.params.data.otherData.timeTo)),
                    endTime: this.props.navigation.state.params.data.otherData.timeFrom == 0 ? 'End Time' : new Date(Number(this.props.navigation.state.params.data.otherData.timeFrom)),
                    endTimeMillisecond: this.props.navigation.state.params.data.otherData.timeTo == 0 ? 0 : new Date(Number(this.props.navigation.state.params.data.otherData.timeTo)).getTime(),
                    startTimeMillisecond: this.props.navigation.state.params.data.otherData.timeFrom == 0 ? 0 : new Date(Number(this.props.navigation.state.params.data.otherData.timeFrom)).getTime(),
                    notes: this.props.navigation.state.params.data.otherData.notes,
                    deletedIndex: this.props.navigation.state.params.data.index,
                    editTimesheet: this.props.navigation.state.params.data.edit,
                    time: this.props.navigation.state.params.data.otherData.hours,
                    date: this.props.navigation.state.params.data.otherData.date ? this.props.navigation.state.params.data.otherData.date : "Date",
                    projectNameEditable: false,
                    activityEditable: false
                }) : null
        }
        if (this.props.navigation.state.params.data) {
            this.props.navigation.state.params.data.activityName ?
                this.setState({
                    activity: this.props.navigation.state.params.data.activityName,
                    activityEditable: false,
                    activityId: this.props.navigation.state.params.data.activityId
                }) : null
        }
        if (this.props.navigation.state.params.data) {
            if (this.props.navigation.state.params.data.timerStop) {
                this.setState({
                    startTime: this.props.navigation.state.params.data.startTime == 0 ? 'Start Time' : new Date(Number(this.props.navigation.state.params.data.startTime)),
                    endTime: this.props.navigation.state.params.data.endTime == 0 ? 'End Time' : new Date(Number(this.props.navigation.state.params.data.endTime)),
                    endTimeMillisecond: this.props.navigation.state.params.data.startTime == 0 ? 0 : new Date(Number(this.props.navigation.state.params.data.startTime)).getTime(),
                    startTimeMillisecond: this.props.navigation.state.params.data.endTime == 0 ? 0 : new Date(Number(this.props.navigation.state.params.data.endTime)).getTime(),
                    //notes: this.props.navigation.state.params.data.otherData.notes,
                    //deletedIndex: this.props.navigation.state.params.data.index,
                    //editTimesheet: this.props.navigation.state.params.data.edit,
                    date: this.props.navigation.state.params.data.date,
                    time: this.props.navigation.state.params.data.hours,
                    //projectNameEditable: false,
                    //activityEditable: false
                })
            }
        }
        AsyncStorage.getItem('userToken').then((token) => {
            //alert(token)
            this.setState({ userToken: token })
            this.hitGetAllActivitiesApi(token)
            this.hitGetAllProjectListApi(token)






        })



    }
    hitGetAllProjectListApi(token) {
        let variables = new FormData();
        //this.setState({ spinnerVisible: true, projectNameMandatory: false })
        variables.append("page", 0)

        return webservice(variables, "project/getlist", 'POST', token)
            .then(response => {
                //this.setState({ spinnerVisible: false })

                if (response != "error") {
                    //alert(JSON.stringify(response))
                    getLog("getProjectList=====++++" + JSON.stringify(response.projectList))
                    if (response.projectList.length != 0) {
                        this.setState({ projectArr: response.projectList, projectArrPassedToSearchFlatList: response.projectList })



                        // projectList
                        // projectListArr=arr

                    }

                    // this.setState({
                    //     activityArr:
                    //         [{ "activityId": "1", "activityName": "Meeting" }, { "activityId": "2", "activityName": "Development" }, { "activityId": "3", "activityName": "Testing" }, { "activityId": "4", "activityName": "Deployment" }, { "activityId": "5", "activityName": "testing11" }, { "activityId": "6", "activityName": "testing123" }, { "activityId": "7", "activityName": "testing456" }, { "activityId": "8", "activityName": "testing789" }, { "activityId": "9", "activityName": "abcd test activity" }, { "activityId": "10", "activityName": "xyz" }]
                    // })


                } else {
                    //this.setState({ spinnerVisible: false })

                }

            })
    }
    hitGetAllActivitiesApi(token) {

        //this.setState({ spinnerVisible: true })


        return webservice('', "activity/getlist", 'POST', token)
            .then(response => {
                //this.setState({ spinnerVisible: false })

                if (response != "error") {
                    //alert(JSON.stringify(response))
                    getLog("getActivityList=====++++" + JSON.stringify(response.activityList))
                    if (response.activityList.length != 0) {
                        this.setState({ activityArr: response.activityList, activityArrPassedToSearchFlatList: response.activityList })



                        // projectList
                        // projectListArr=arr

                    }

                    // this.setState({
                    //     activityArr:
                    //         [{ "activityId": "1", "activityName": "Meeting" }, { "activityId": "2", "activityName": "Development" }, { "activityId": "3", "activityName": "Testing" }, { "activityId": "4", "activityName": "Deployment" }, { "activityId": "5", "activityName": "testing11" }, { "activityId": "6", "activityName": "testing123" }, { "activityId": "7", "activityName": "testing456" }, { "activityId": "8", "activityName": "testing789" }, { "activityId": "9", "activityName": "abcd test activity" }, { "activityId": "10", "activityName": "xyz" }]
                    // })


                } else {
                    //this.setState({ spinnerVisible: false })

                }

            })
    }
    startSearchActivity(text, type) {

        var arr = []
        var typeArr = type == "project" ? this.state.projectArr : this.state.activityArr
        for (var i = 0; i < typeArr.length; i++) {
            if (type == "project") {
                if (typeArr[i].projectName.toLowerCase().indexOf(text.toLowerCase()) != -1) {
                    arr.push(typeArr[i])
                }
            } else {
                if (typeArr[i].activityName.toLowerCase().indexOf(text.toLowerCase()) != -1) {
                    arr.push(typeArr[i])
                }
            }

        }
        type == "project" ?
            this.setState({ projectArrPassedToSearchFlatList: arr })
            :
            this.setState({ activityArrPassedToSearchFlatList: arr })
        if (arr.length == 0) {
            this.setState({ activityTextFieldWrites: false, projectTextFieldWrites: false })
        }

        //alert(this.state.activityArr[i].activityName.includes(text))


    }
    changeText(text, placeHolder) {

        if (placeHolder == 'Project Name') {
            if (this.state.projectArr.length > 0) {
                this.startSearchActivity(text, "project")
                this.setState({ projectTextFieldWrites: true })
                // if (text.length > 0) {
                //     if (this.state.projectArrPassedToSearchFlatList.length == 0) {
                //         this.setState({ projectTextFieldWrites: false })
                //     } else {
                //         this.setState({ projectTextFieldWrites: true })
                //     }

                // } else {
                //     this.setState({ projectTextFieldWrites: false })
                // }
            }
            this.setState({ name: text, projectId: '' })
        }
        if (placeHolder == 'Activity') {
            if (this.state.activityArr.length > 0) {
                this.startSearchActivity(text, 'activity')
                // if (text.length > 0) {
                //     if (this.state.activityArrPassedToSearchFlatList.length == 0) {
                //         this.setState({ activityTextFieldWrites: false })
                //     } else {
                //         this.setState({ activityTextFieldWrites: true })
                //     }

                // } else {
                //     this.setState({ activityTextFieldWrites: false })
                // }
                this.setState({ activityTextFieldWrites: true })
            }

            this.setState({ activity: text })

        }
        if (placeHolder == 'Notes') {
            this.setState({ notes: text })
        }
        if (placeHolder == 'Location') {
            this.setState({ location: text })
        }


    }
    textInput(placeHolder, multiline, value, focus, editable) {
        return (
            <TextInput
                style={styles.textInput}
                placeholder={placeHolder}
                placeholderTextColor="#333333"
                multiline={multiline}
                editable={editable}
                onFocus={() => focus != null ? focus == "project" ? this.setState({ activityTextFieldWrites: false, projectTextFieldWrites: true, scrollable: false }) : this.setState({ projectTextFieldWrites: false, activityTextFieldWrites: true, scrollable: false }) : null}
                //numberOfLines={5}
                onBlur={()=>Keyboard.dismiss()}
                //editable={placeHolder == 'Project Name' ? this.state.name == '' ? true : false : true}
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.changeText(text, placeHolder)}
                value={value}
            />
        )
    }
    hitAddActivityAPI() {
        let variables = new FormData();
        variables.append("activityName", this.state.activity)

        return webservice(variables, "activity/add", 'POST', this.state.userToken)
            .then(response => {
                if (response != "error") {
                    getLog("Activity added" + JSON.stringify(response))
                    this.setState({ activityId: response.activityData.activityId })

                }

            })
    }
    hitAddTimeSheetAPI() {
        let variables = new FormData();
        variables.append("projectId", this.state.projectId)
        variables.append("activityId", this.state.activityId)
        variables.append("startTime", this.state.startTime == "Start Time" ? 0 : new Date(this.state.startTime).getTime())
        variables.append("endTime", this.state.endTime == "End Time" ? 0 : new Date(this.state.endTime).getTime())
        //variables.append("totalHours", this.state.time)
        variables.append("totalHours", this.state.time)
        variables.append("billable", this.state.billable)
        variables.append("location", this.state.location)
        variables.append("latitude", this.state.latitude)
        variables.append("longitude", this.state.longitude)
        variables.append("notes", this.state.notes)
        variables.append("addedDate", this.state.date)

        return webservice(variables, "timesheet/add", 'POST', this.state.userToken)
            .then(response => {
                if (response != "error") {
                    //alert(response.message)
                    getLog("Activity added" + JSON.stringify(response))
                    this.goBackToPreviousPageAndSentCallBack()


                }

            })
    }

    hitEditTimeSheetAPI() {
        let variables = new FormData();
        variables.append("projectId", this.state.projectId)
        variables.append("timesheetId", this.props.navigation.state.params.data.otherData.timesheetId)
        variables.append("activityId", this.state.activityId)
        variables.append("startTime", this.state.startTime == "Start Time" ? 0 : new Date(this.state.startTime).getTime())
        variables.append("endTime", this.state.endTime == "End Time" ? 0 : new Date(this.state.endTime).getTime())
        //variables.append("totalHours", this.state.time)
        variables.append("totalHours", this.state.time)
        variables.append("billable", this.state.billable)
        variables.append("location", this.state.location)
        variables.append("latitude", this.state.latitude)
        variables.append("longitude", this.state.longitude)
        variables.append("notes", this.state.notes)
        variables.append("addedDate", this.state.date)

        return webservice(variables, "timesheet/edit", 'POST', this.state.userToken)
            .then(response => {
                if (response != "error") {
                    //alert(response.message)
                    //getLog("Activity added" + JSON.stringify(response))
                    this.goBackToPreviousPageAndSentCallBack()


                }

            })
    }

    goBackToPreviousPageAndSentCallBack() {
        var data = {
            projectName: this.state.name,
            activity: this.state.activity,
            notes: this.state.notes,
            billable: this.state.billable,
            time: this.state.time,
            index: this.state.deletedIndex,
            edit: this.state.editTimesheet,
            startTime: this.state.startTimeMillisecond != 0 ? new Date(this.state.startTimeMillisecond).getTime() : this.state.startTimeMillisecond,
            endTime: this.state.endTimeMillisecond != 0 ? new Date(this.state.endTimeMillisecond).getTime() : this.state.endTimeMillisecond,
        }
        this.props.navigation.state.params.data.returnCallBack.callBack(this.state.userToken, this.state.filterData)
        this.props.navigation.goBack()
    }
    openAlertForAddNewProject() {

        Alert.alert(
            'Create Project?',
            'The project name you entered does not exist. \nDo you want to create a new project?',
            [

                { text: 'Cancel', onPress: () => this.setState({ name: '' }), style: 'cancel' },
                { text: 'OK', onPress: () => this.openModalForAddNewProject() },
            ],
            { cancelable: false }
        )


        // alert("new")
    }
    openModalForAddNewProject() {
        this.setState({
            projectModalTitleText: "New Project",
            projectNameModalText: '',
            projectCodeModalText: '',
            startDateToShowOnMOdal: '',
            endDateToShowOnModal: '',
            projectBillingModalText: '',
            projectCurrencyModalText: 'Currency',
            projectNameMandatory: false,
            projectNameMandatoryText: ''

        })
        setTimeout(() => {
            this.setState({ projectModalVisible: true, projectNameModalText: this.state.name })
        }, 300)

    }
    saveClick() {

        if (this.state.name == '') {
            alert("Please enter project name.")
            return
        } else if (this.state.activity == '') {
            alert("Please enter Activity.")
            return
        } else if (this.state.date == 'Date') {
            alert("Please select date.")
            return
        }
        else if (this.state.startTime == "Start Time" && this.state.endTime == 'End Time') {
            if (this.state.time == 0) {
                alert("Please select either total time or start and end time.")
                return
            }
        } else if (this.state.endTime == 'End Time') {
            alert("Please select end time.")
            return
        }



        var flag = false
        for (var i = 0; i < this.state.activityArr.length; i++) {
            if (this.state.activityArr[i].activityName.toLowerCase() == this.state.activity.toLowerCase()) {
                flag = true
                break
            } else {
                flag = false
            }
        }
        if (!flag) {
            this.hitAddActivityAPI()
            //Hit add activity API
        }

        var projectFlag = false
        for (var i = 0; i < this.state.projectArr.length; i++) {
            if (this.state.projectArr[i].projectName.toLowerCase() == this.state.name.toLowerCase()) {
                projectFlag = true
                break
            } else {
                projectFlag = false
            }
        }
        if (!projectFlag) {
            this.openAlertForAddNewProject()
            //Hit add activity API
        }
        //This code belongs to that save api hit only if the project exist in the project list
        //if project doesnot exist then first modal opens then project adds after thet project id is
        //returned that is added to project id then this add api hits
        if (projectFlag) {
            if (this.state.deletedIndex != null && !this.state.editTimesheet) {
                //alert("delete")
                //delete case
            }
            else {
                if (this.state.editTimesheet) {
                    //edit case
                    setTimeout(()=>{
                        this.hitEditTimeSheetAPI()
                    },500)
                    
                    //alert("eidt")
                } else {
                    setTimeout(()=>{
                        this.hitAddTimeSheetAPI()
                    },500)
                    
                    //alert("new subactivity added")
                    //new add
                }
            }
        }

        // var data = {
        //     projectName: this.state.name,
        //     activity: this.state.activity,
        //     notes: this.state.notes,
        //     billable: this.state.billable,
        //     time: this.state.time,
        //     index: this.state.deletedIndex,
        //     edit: this.state.editTimesheet,
        //     startTime: this.state.startTimeMillisecond != 0 ? new Date(this.state.startTimeMillisecond).getTime() : this.state.startTimeMillisecond,
        //     endTime: this.state.endTimeMillisecond != 0 ? new Date(this.state.endTimeMillisecond).getTime() : this.state.endTimeMillisecond,
        // }
        // this.props.navigation.state.params.data.returnCallBack.callBack(data)
        // this.props.navigation.goBack()



        // alert(this.state.name + this.state.activity + this.state.notes)
    }
    Delete() {


        Alert.alert(
            'Delete',
            'Are you sure you want to delete this activity?',
            [

                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => this.deleteOkPress() },
            ],
            { cancelable: false }
        )

    }
    deleteOkPress() {
        // var data = { projectName: this.state.name, activity: this.state.activity, notes: this.state.notes, billable: true, time: this.state.time, index: this.state.deletedIndex, edit: false }
        // this.props.navigation.state.params.data.returnCallBack.callBack(data)
        // this.props.navigation.goBack()
    }
    modalConfirmPress() {
        this.setState({
            timeModalVisible: false, time: (this.state.hourText * 60) + this.state.minuteText,
            startTime: 'Start Time', startTimeMillisecond: 0, endTime: 'End Time', endTimeMillisecond: 0
        })

    }
    hourUpBTn() {
        this.setState({ hourText: this.state.hourText + 1 })
    }
    hourDownBTn() {
        this.state.hourText >= 1 ?
            this.setState({ hourText: this.state.hourText - 1 })
            : null
    }
    minDownBTn() {
        this.state.minuteText >= 1 ?
            this.setState({ minuteText: this.state.minuteText - 1 })
            : this.setState({ minuteText: 0 })
    }

    minUpBTn() {
        this.state.minuteText <= 58 ?
            this.setState({ minuteText: this.state.minuteText + 1 })
            : this.setState({ minuteText: 0 })
    }

    // onTimeSelect(time) {

    //     this.setState({ timePickerVisible: false })

    //     var millisecond = new Date(time);
    //     var d = millisecond.getHours() + ":" + millisecond.getMinutes();
    //     if (this.state.timePickerType == "start") {
    //         this.setState({ startTimeMillisecond: new Date(time).getTime(), endTime: "End Time" })

    //         setTimeout(() => {
    //             if (this.state.endTime != '' && this.state.endTime != 'End Time') {
    //                 if (this.state.startTimeMillisecond > this.state.endTimeMillisecond) {
    //                     setTimeout(() => {
    //                         alert("End time must be greater than start time")
    //                     }, 500)

    //                 } else {
    //                     this.setState({ startTime: time, })
    //                 }
    //             } else {
    //                 this.setState({ startTime: time, startTimeMillisecond: new Date(time).getTime() })
    //             }
    //         }, 500)


    //     } else {
    //         this.setState({ endTimeMillisecond: new Date(time).getTime() })
    //         setTimeout(() => {
    //             if (this.state.startTimeMillisecond > this.state.endTimeMillisecond) {
    //                 setTimeout(() => {
    //                     alert("End time must be greater than start time")
    //                 }, 500)
    //             } else {
    //                 this.setState({ endTime: time, })
    //                 setTimeout(() => {
    //                     var diff = Math.round((this.state.endTimeMillisecond - this.state.startTimeMillisecond) / 60000)

    //                     this.setState({ time: diff })
    //                     //this.calculateHoursMinutes(diff)
    //                 }, 200)
    //             }
    //         }, 500)

    //     }


    // }

    onTimeSelect(time) {

        this.setState({ timePickerVisible: false })

        var millisecond = new Date(time);
        var d = millisecond.getHours() + ":" + millisecond.getMinutes();
        if (this.state.timePickerType == "start") {
            this.setState({ startTimeMillisecond: new Date(time).getTime(), endTime: "End Time" })

            setTimeout(() => {
                if (this.state.endTime != '' && this.state.endTime != 'End Time') {
                    if (this.state.startTimeMillisecond > this.state.endTimeMillisecond) {
                        setTimeout(() => {
                            alert("End time must be greater than start time")
                        }, 500)

                    } else {
                        this.setState({ startTime: time, })
                    }
                } else {
                    this.setState({ startTime: time, startTimeMillisecond: new Date(time).getTime() })
                }
            }, 500)


        } else {
            this.setState({ endTimeMillisecond: new Date(time).getTime() })
            setTimeout(() => {
                if (this.state.startTimeMillisecond > this.state.endTimeMillisecond) {
                    setTimeout(() => {
                        alert("End time must be greater than start time")
                    }, 500)
                } else {
                    this.setState({ endTime: time, })
                    setTimeout(() => {
                        var diff = Math.round((this.state.endTimeMillisecond - this.state.startTimeMillisecond) / 60000)

                        this.setState({ time: diff })
                        //this.calculateHoursMinutes(diff)
                    }, 200)
                }
            }, 500)

        }


    }
    onDateSelect(date) {
        this.setState({ date: new Date(date).getTime(), datePickerVisible: false })

    }

    // calculateHoursMinutes(duration) {
    //     //var milliseconds = parseInt((duration % 1000) / 100)
    //     //ar seconds = parseInt((duration / 1000) % 60)
    //     var minutes = parseInt((duration / (1000 * 60)) % 60)
    //     var hours = parseInt((duration / (1000 * 60 * 60)) % 24)
    //     this.setState({ time: hours + "h " + minutes + "m" })
    //     //alert(hours + minutes)
    // }

    onPressTimePickerVisible(type) {
        if (type == "start") {
            this.setState({ timePickerVisible: true, timePickerType: type })
        } else {
            if (this.state.startTime != '' && this.state.startTime != 'Start Time') {
                this.setState({ timePickerVisible: true, timePickerType: type })
            } else {
                alert("Please select start time first")
            }
        }

    }
    activitySearchView() {
        return (
            <View style={{
                maxHeight: 150, width: width - 40, zIndex: 99, borderColor: "#E5E5E5", borderWidth: this.state.activityArrPassedToSearchFlatList.length > 0 ? 1 : 0,
                left: 10, right: 15, position: "absolute", top: Platform.OS == 'ios' ? 212 : 258, backgroundColor: "white"
            }}>
                <FlatList
                    data={this.state.activityArrPassedToSearchFlatList}
                    //data={this.state.tabId == "Projects" ? this.state.projectList : this.state.tabId == "Leave Types" ? this.state.leaveTypeList : this.state.activitiList}
                    renderItem={({ item, index }) => this.createList(item, index)}
                    keyExtractor={key => key.index}
                    extraData={this.state}
                    keyboardShouldPersistTaps="handled"
                />
            </View>
        )

    }
    projectSearchView() {
        return (
            <View style={{
                maxHeight: 150, width: width - 40, zIndex: 99, borderColor: "#E5E5E5", borderWidth: this.state.projectArrPassedToSearchFlatList.length > 0 ? 1 : 0,
                left: 10, right: 15, position: "absolute", top: Platform.OS == 'ios' ? 50 : 75, backgroundColor: "white"
            }}>
                <FlatList
                    data={this.state.projectArrPassedToSearchFlatList}
                    //data={this.state.tabId == "Projects" ? this.state.projectList : this.state.tabId == "Leave Types" ? this.state.leaveTypeList : this.state.activitiList}
                    renderItem={({ item, index }) => this.createProjectList(item, index)}
                    keyExtractor={key => key.index}
                    extraData={this.state}
                    keyboardShouldPersistTaps="handled"
                />
            </View>
        )

    }
    activitySelectedFromDropdown(item) {
        Keyboard.dismiss()
        this.setState({ activity: item.activityName, activityTextFieldWrites: false, activityId: item.activityId })
    }

    projectSelectedFromDropdown(item) {
        Keyboard.dismiss()
        this.setState({ name: item.projectName, projectTextFieldWrites: false, projectId: item.projectId })
    }
    createList(item, index) {
        return (
            <TouchableOpacity key={"abc" + index} style={{zIndex:100}}
                onPress={() => this.activitySelectedFromDropdown(item)}>
                <View style={{ padding: 10, paddingLeft: 0, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                    <Text style={{ fontSize: fontSizes.font14, color: "#333333" }}>
                        {item.activityName}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
    createProjectList(item, index) {
        return (
            <TouchableOpacity key={"abcww" + index} style={{zIndex:100}}
                onPress={() => this.projectSelectedFromDropdown(item)}>
                <View style={{ padding: 10, paddingLeft: 0, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                    <Text style={{ fontSize: fontSizes.font14, color: "#333333" }}>
                        {item.projectName}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
    keyboradDismiss() {
        Keyboard.dismiss()
        this.setState({ activityTextFieldWrites: false, projectTextFieldWrites: false, scrollable: true })

    }

    allowLocation() {
        Alert.alert(
            'Simplitime',
            'Simplitime wants to access your location',
            [

                { text: "Don't allow", onPress: () => this.cancelLocation(), style: 'cancel' },
                { text: 'Allow', onPress: () => this.getLocation() },
            ],
            { cancelable: false }
        )
    }
    cancelLocation() {
        this.setState({ showLocationTextField: false, latitude: '', longitude: '', location: '' })
    }
    getLocation() {
        this.setState({ spinnerVisible: true })
        setTimeout(() => {
            if (this.state.spinnerVisible) {
                this.setState({ spinnerVisible: false })
                setTimeout(() => {
                    alert("Simplitime is not be able to find your location.Fill your location manually.")
                    this.setState({ showLocationTextField: true, latitude: '', longitude: '' })
                }, 300)
            }
        }, 15000)
        //getLog("------")



        //alert(JSON.stringify(navigator.geolocation))
        navigator.geolocation.getCurrentPosition(
            (position) => {
                //alert(JSON.stringify(position))
                //   this.setState({
                //     latitude: position.coords.latitude,
                //     longitude: position.coords.longitude,
                //     error: null,
                //   });
                // alert(position.coords.latitude + "==" + position.coords.longitude)
                var key = Platform.OS == 'ios' ? 'AIzaSyCaEHpRii73SWa0atFPwv73RCYIMTCPicU' : 'AIzaSyD6R0ummmDrLNT22KXfdmcHRjz2i5N40Cw'
                fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + position.coords.latitude + ',' + position.coords.longitude + '&key=AIzaSyCaEHpRii73SWa0atFPwv73RCYIMTCPicU')
                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({ spinnerVisible: false })
                        getLog('ADDRESS GEOCODE is BACK!! => ' + JSON.stringify(responseJson));
                        this.setState({
                            location: responseJson.results[2].formatted_address,
                            showLocationTextField: true,
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude
                        })
                        //alert('===== => ' + responseJson.results[2].formatted_address);

                    })

            },
            //(error) => this.setState({ error: error.message }),
            (error) => getLog(error.message),
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 60000 },
        );
    }
    projectModalSaveButtonClicked() {
        this.setState({ minDate: '' })



        if (this.state.projectNameModalText != '') {

            if (this.state.projectBillingModalText == '' && this.state.projectCurrencyModalText == "Currency") {
                this.addProjectAPI()
            } else if (this.state.projectBillingModalText != '' && this.state.projectCurrencyModalText != "Currency") {
                this.addProjectAPI()
            } else {
                this.state.projectBillingModalText == '' ?
                    this.setState({ projectNameMandatory: true, projectNameMandatoryText: "*Please enter Billing rate in per hour." })
                    :
                    this.setState({ projectNameMandatory: true, projectNameMandatoryText: "*Please enter currency." })


            }


        } else {
            this.setState({ projectNameMandatory: true, projectNameMandatoryText: "*Please enter project name" })
        }


        // this.setState({ projectList: arr })
        // setTimeout(() => {
        //     this.setState({ projectModalVisible: false })
        //     this.setState({
        //         projectNameModalText: '',
        //         projectCodeModalText: '',
        //         spinnerVisible: false,
        //         projectBillingModalText: '',
        //         projectCurrencyModalText: '',
        //         startDate: '',
        //         endDate: ''
        //     })
        // }, 300)
    }
    addProjectAPI() {

        let variables = new FormData();
        this.setState({ spinnerVisible: true, projectNameMandatory: false, projectNameMandatoryText: '' })
        variables.append("projectName", this.state.projectNameModalText)
        variables.append("projectCode", this.state.projectCodeModalText)
        if (this.state.startDate != '') {
            variables.append("startDate", new Date(this.state.startDate).getTime())
        } else {
            variables.append("startDate", '')
        }
        if (this.state.endDate != '') {
            variables.append("endDate", new Date(this.state.endDate).getTime())
        } else {
            variables.append("endDate", '')
        }

        variables.append("billing", this.state.projectBillingModalText)
        variables.append("currency", this.state.projectCurrencyModalText == "Currency" ? "" : this.state.projectCurrencyModalText)
        getLog("=====" + this.state.userToken)
        return webservice(variables, "project/add", 'POST', this.state.userToken)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {
                    //this.hitGetAllProjectListApi(this.state.userToken)
                    var arr = this.state.projectArr
                    //response.projectData.projectId
                    arr.push({ projectName: this.state.name, projectId: response.projectData.projectId })
                    this.setState({ projectArr: arr, projectId: response.projectData.projectId })

                    setTimeout(() => {
                        this.saveClick()
                        this.setState({ projectModalVisible: false })



                        this.setState({
                            projectNameModalText: '',
                            projectCodeModalText: '',
                            spinnerVisible: false,
                            projectBillingModalText: '',
                            projectCurrencyModalText: '',
                            startDate: '',
                            endDate: '',
                            projectNameMandatory: false,
                            projectNameMandatoryText: ''
                        })

                    }, 300)
                }

            })
    }
    startDateVisibleFunction() {
        this.setState({ projectModalDatePickerVisible: true, datePickerType: 'start', startDate: '' })
    }
    endDateVisibleFunction() {
        if (this.state.startDate == '') {
            alert("Please select start date first")
        } else {
            this.setState({ projectModalDatePickerVisible: true, datePickerType: 'end', minDate: this.state.startDate })
        }

    }
    getDateFromModal(date) {
        //alert(date)startDateToShowOnMOdal
        //this.setState({ minDate: date })
        if (this.state.datePickerType == 'start') {
            this.setState({ startDate: date, datePickerType: '', projectModalDatePickerVisible: false, endDateToShowOnModal: '', endDate: '' })
            var d = dateConverter(date)
            setTimeout(() => {
                this.setState({ startDateToShowOnMOdal: d, })
            }, 200)
        } if (this.state.datePickerType == 'end') {

            if (date < new Date(this.state.startDate)) {


                alert("End date should be greater than start sate")


            } else {
                this.setState({ endDate: date, datePickerType: '', projectModalDatePickerVisible: false, minDate: '' })
                var d = dateConverter(date)
                setTimeout(() => {
                    this.setState({ endDateToShowOnModal: d })
                }, 200)
            }

        }
    }
    render() {

        return (
            <SafeAreaView style={styles.safeArea}>
                <Spinner visible={this.state.spinnerVisible} />
                <TouchableWithoutFeedback onPress={() => this.keyboradDismiss()}>

                    <View style={styles.container}>
                        <Header
                            HeaderLeftText={this.state.editTimesheet ? "Edit Timesheet" : "Create Timesheet"}
                            headerType='back'
                            HeaderRightText='Save'
                            save={true}
                            style={{ paddingLeft: 10 }}
                            toggleDrawer={() => this.props.navigation.goBack()}
                            //headerLeftIcon={}
                            headerRightIcon={require('../images/headerIcons/tickIcon.png')}
                            headerRightIconOnPress={() => this.saveClick()}
                        />
                        <TimeModal
                            modalVisible={this.state.timeModalVisible}
                            modalClose={() => this.setState({ timeModalVisible: false })}
                            hourUpBTn={() => this.hourUpBTn()}
                            hourDownBTn={() => this.hourDownBTn()}
                            minDownBTn={() => this.minDownBTn()}
                            minUpBTn={() => this.minUpBTn()}
                            onConfirmPress={() => this.modalConfirmPress()}
                            hourText={this.state.hourText}
                            minuteText={this.state.minuteText}
                        />
                        <DateTimePicker
                            isVisible={this.state.timePickerVisible}
                            onConfirm={(time) => this.onTimeSelect(time)}
                            mode='time'
                            //showTime = {{ user24hours: true }} 
                            titleIOS="Pick a time"
                            locale="NL"
                            //minimumDate={minDate}
                            onCancel={() => this.setState({ timePickerVisible: false })}
                        />



                        {/* project modal in case of user enters project that is not in list */}
                        <ProjectModal
                            modalVisible={this.state.projectModalVisible}
                            modalClose={() => this.setState({ projectModalVisible: false })}
                            TitleText="New Project"
                            ProjectNameValue={this.state.projectNameModalText}
                            projectNameChangeText={(text) => this.setState({ projectNameModalText: text, projectNameMandatory: false, projectNameMandatoryText: '' })}
                            ProjectCodeValue={this.state.projectCodeModalText}
                            projectCodeChangeText={(text) => this.setState({ projectCodeModalText: text })}
                            BillingValue={this.state.projectBillingModalText}
                            billingTextInputData={(text) => this.setState({ projectBillingModalText: text, projectNameMandatory: false, projectNameMandatoryText: '' })}
                            Currency={this.state.projectCurrencyModalText}

                            saveButtonClicked={() => this.projectModalSaveButtonClicked()}
                            datePickerVisible={this.state.projectModalDatePickerVisible}
                            startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
                            datePickerCloseFunction={() => this.setState({ projectModalDatePickerVisible: false })}
                            endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
                            dateSelected={(date) => this.getDateFromModal(date)}
                            startDate={this.state.startDateToShowOnMOdal}
                            endDate={this.state.endDateToShowOnModal}
                            minDate={this.state.minDate == '' ? new Date('01/01/1990') : this.state.minDate}
                            CurrencyOptionArrayOnProjectModal={["None", "$", "€", "£", "₹"]}
                            selectCurrencyClicked={(index, value) => this.setState({ projectCurrencyModalText: value == "None" ? "Currency" : value, projectNameMandatory: false, projectNameMandatoryText: '' })}
                            projectNameMandatory={this.state.projectNameMandatory}
                            projectNameMandatoryText={this.state.projectNameMandatoryText}
                        />


                        {/* //end of project modal in case that user enters project that is not in list */}


                        <DateTimePicker
                            isVisible={this.state.datePickerVisible}
                            onConfirm={(date) => this.onDateSelect(date)}
                            mode='date'
                            titleIOS="Pick a date"
                            //minimumDate={minDate}
                            onCancel={() => this.setState({ datePickerVisible: false })}
                        />
                        <ScrollView
                            scrollEnabled={this.state.scrollable}
                            keyboardShouldPersistTaps="always"
                            style={[styles.mainContainer, { paddingBottom: this.state.activityTextFieldWrites ? 70 : 0 }]}>


                            {this.textInput("Project Name", false, this.state.name, "project", !this.state.editTimesheet)}

                            {this.state.projectTextFieldWrites ? this.projectSearchView() : null}
                            <TouchableOpacity onPress={() => this.setState({ datePickerVisible: true })}
                                style={[styles.clockView, { marginTop: 20, width: "40%" }]}>
                                <Text style={{ fontSize: fontSizes.font14, }}>
                                    {this.state.date == "Date" ? this.state.date : dateConverter(new Date(this.state.date))}
                                </Text>
                                <Image source={require('../images/createTimesheet/dateIcon.png')} />
                            </TouchableOpacity>
                            <View style={[styles.rowView, { marginBottom: 5 }]}>
                                <TouchableOpacity onPress={() => this.onPressTimePickerVisible("start")}
                                    style={styles.clockView}>
                                    <Text style={{ fontSize: fontSizes.font14, }}>
                                        {this.state.startTime == 'Start Time' ? this.state.startTime : this.state.startTime.getHours() + ":" + this.state.startTime.getMinutes()}
                                    </Text>
                                    <Image source={require('../images/createTimesheet/greenClock.png')} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.onPressTimePickerVisible("end")}
                                    style={styles.clockView}>
                                    <Text style={{ fontSize: fontSizes.font14, }}>
                                        {this.state.endTime == 'End Time' ? this.state.endTime : this.state.endTime.getHours() + ":" + this.state.endTime.getMinutes()}
                                    </Text>
                                    <Image source={require('../images/createTimesheet/redClock.png')} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({ timeModalVisible: true })}
                                    style={{ flex: 1, justifyContent: "space-between", alignItems: "center", }}>
                                    <Text style={{ color: "#2A56C6", fontSize: fontSizes.font16 }}>
                                        {hourMinuteConverter(this.state.time)}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            {this.textInput("Activity", false, this.state.activity, "activity", !this.state.editTimesheet)}


                            {this.state.activityTextFieldWrites ? this.activitySearchView() : null}




                            <View style={[styles.rowView,]}>
                                <TouchableWithoutFeedback onPress={() => this.setState({ billable: !this.state.billable })}>
                                    <View style={{ flexDirection: "row", flex: 1, marginHorizontal: 20 }}>
                                        <Image source={this.state.billable ? require('../images/createTimesheet/tickIcon.png') : require('../images/createTimesheet/uncheckIcon.png')} />
                                        <Text style={{ fontSize: fontSizes.font14, marginLeft: 15 }}>
                                            Billable
                                </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => this.allowLocation()}>
                                    <View style={{ flexDirection: "row", flex: 1 }}>
                                        <Image source={require('../images/createTimesheet/locationIcon.png')} />
                                        <Text style={{ fontSize: fontSizes.font14, marginLeft: 15 }}>
                                            Location
                                </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                            {this.state.showLocationTextField ? this.textInput("Location", true, this.state.location, null, true) : null}

                            {this.textInput("Notes", true, this.state.notes, null)}

                            {this.state.editTimesheet ?
                                <TouchableWithoutFeedback onPress={() => this.Delete()}>
                                    <View style={styles.buttonStyle} >
                                        <Text style={[styles.titleText, {}]}>
                                            Delete
                                </Text>
                                    </View>
                                </TouchableWithoutFeedback> : null}


                        </ScrollView>


                    </View>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#fff'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    mainContainer: { flex: 1, marginHorizontal: 15 },
    textInput: { borderBottomColor: "#D8D8D8", borderBottomWidth: 1, marginTop: Platform.OS == "ios" ? 20 : 15, color: '#333333', fontSize: fontSizes.font14, paddingBottom: 10, paddingLeft: 10 },
    clockView: { flex: 1.5, justifyContent: "space-between", alignItems: "center", flexDirection: "row", marginRight: 20, paddingLeft: 10, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, paddingBottom: 10 },
    rowView: { flexDirection: "row", justifyContent: "space-between", marginTop: 25, marginBottom: 10 },
    titleText: { marginLeft: 5, color: 'white', textAlign: 'center', fontSize: fontSizes.font16, marginTop: 0 },
    buttonStyle: { backgroundColor: "#2A56C6", height: 50, alignItems: "center", justifyContent: 'center', marginTop: 50, borderRadius: 3 }
});
