/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, StatusBar, Keyboard, TouchableWithoutFeedback,
    Text, SafeAreaView, TouchableOpacity,
    View, Image, TextInput, ScrollView
} from 'react-native';
import Header from '../components/commonHeader'
import { height, width, fontSizes } from '../utils/utils'


export default class forgotPassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: ''
        }
    }

    render() {

        return (
            <SafeAreaView style={styles.safeArea}>
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <View style={styles.container}>
                        <Header
                            HeaderLeftText="Forgot Password"
                            headerType='back'
                            //HeaderRightText=''

                            toggleDrawer={() => this.props.navigation.goBack()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        //headerRightIcon={require('../images/drawerIcons/editIcon.png')}
                        //headerRightIconOnPress={} 
                        />
                        <View style={styles.mainContainer}>
                            <Text style={styles.titleText}>
                                Email Id
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                placeholder="Email"
                                value={this.state.email}
                                placeholderTextColor="#333333"
                                underlineColorAndroid="transparent"
                                //maxLength={placeholder == "Company Id" ? 5 : 100}
                                //secureTextEntry={placeholder == "Password" ? true : false}
                                onChangeText={(text) => this.setState({ email: text })}
                            //value={ProjectCodeValue}
                            />
                            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate("ResetPassword")}>
                                <View style={styles.buttonStyle} >
                                    <Text style={[styles.titleText, { color: 'white', textAlign: 'center', fontSize: fontSizes.font16, marginTop: 0 }]}>
                                        RESET PASSWORD
                                </Text>
                                </View>
                            </TouchableWithoutFeedback>

                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#fff'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    mainContainer: {
        position: "absolute", width: width - 20,
        left: 10, right: 10, backgroundColor: "#fff",
        top: 80,
        elevation: 5,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: .5,
        padding: 15, borderRadius: 3
    },
    titleText: { fontSize: fontSizes.font14, color: "#898989", marginTop: 15, marginLeft: 5 },
    textInput: { height: 40, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, color: '#333333', fontSize: fontSizes.font16, marginHorizontal: 5, marginTop: 5 },
    buttonStyle: { backgroundColor: "#2A56C6", height: 50, alignItems: "center", justifyContent: 'center', marginTop: 30, marginBottom: 20, borderRadius: 3 }

});

