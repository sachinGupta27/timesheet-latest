/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
     Image
} from 'react-native';

import Setting from './setting'

export default class manageProject extends Component {
    static navigationOptions = {
        drawerLabel: 'Manage Project',
        drawerIcon: ({ tintColor }) => (
            <Image style={{tintColor: tintColor}}
            source={require('../images/drawerIcons/manageProjectIcon.png')} />
        ),
    };
    render() {
        return (
            <Setting id="manageProject" navigation={this.props.navigation}/>
        );
    }
}

