/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet, SafeAreaView, AsyncStorage,
  Text, TouchableWithoutFeedback, LayoutAnimation,
  View, Image, ScrollView, TouchableOpacity, FlatList
} from 'react-native';

import CustomDrawer from '../components/customDrawer'

import Header from '../components/commonHeader'
import DateTimePicker from 'react-native-modal-datetime-picker';
import FloatingButton from '../components/floatingButton'
import TimeLeaveTab from '../components/timesheetLeaveTab'
import BarGraph from '../components/barGraph'
import PickerDropdownModal from '../components/pickerDropdownModal'
import SelectProjectModal from '../components/selectProjectModal'
import { height, width, fontSizes, colorArray, dynamicSize, getLog } from '../utils/utils'
import webservice from '../components/webService'
import Spinner from '../components/spinner'
import { dateConverterUseInHeader, hourMinuteConverter, today_To_day_AfterEighth_Day, week_Date_Array } from '../components/dateConverter'
import PieChart from 'react-native-pie-chart';
const chart_wh = width / 2
const series = [60, 4, 16, 20]
const sliceColor = colorArray
const billableData = ["All", "Billable"]
//import { VictoryBar, VictoryPie } from "victory-native";
export default class analysisExport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TabPress: true,
      showSummary: false,
      projectList: [],
      leaveList: [],
      showProjects: false,
      activityList: [],
      showActivity: false,
      startDateTimePickerVisible: false,
      endDateTimePickerVisible: false,
      startDate: week_Date_Array[26].startDate,
      endDate: week_Date_Array[26].endDate,
      datePassedToHeader: '',
      pickerDropdownModalVisible: false,
      pickerDropdownModalTitleText: "Select Project",
      type: '',
      selectProject: "All Project",
      billableAmount: "Billable",
      leaveType: "All",
      showBarGraph: false,
      selectProjectModal: false,
      customProjectListForModal: [],
      customProjectListForModalThatNotChanges: [],
      totalProjectList: [],
      totalActivityList: [],
      projectViewMoreVisible: true,
      activityViewMoreVisible: true,
      spinnerVisible: false,
      projectTotalTimeSummary: '',
      projectDailyAvgHourSummary: '',
      projectTotalBillingSummary: '',

      leaveTotalAccuredSummary: '',
      leaveTotalBalanceSummary: '',
      leaveTotalAvailedSummary: '',

      leaveTypeListPassedToLeaveModal: []



    }
  }
  static navigationOptions = {
    drawerLabel: 'Analysis & Export',
    drawerIcon: ({ tintColor }) => (
      <Image style={{ tintColor: tintColor }}
        source={require('../images/drawerIcons/analysisIcon.png')} />
    ),
  };
  componentWillMount() {
    this.hitAllApi()
    var projectArr = [
      { "projectName": "ProjectXYZ asd asdas bsbs sbsb sbsb ", 'price': 2, 'progress': 80, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 5, 'progress': 40, 'time': "7h 20m" },
      { "projectName": "ProjectXYZ", 'price': 6, 'progress': 60, 'time': "4h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 10, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 45, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 80, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 5, 'progress': 40, 'time': "7h 20m" },
      { "projectName": "ProjectXYZ", 'price': 6, 'progress': 60, 'time': "4h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 10, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 45, 'time': "10h 20m" },

    ]

    var leaveArr = [
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Planned Leave", 'progress': 80, 'totalDays': 4 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 5 },

    ]

    var activityArr = [
      { 'activity': "Activity 1", 'time': "10h 20m" },
      { 'activity': "Activity 2", 'time': "10h 20m" },
      { 'activity': "Activity 3", 'time': "10h 20m" },
      { 'activity': "Activity 4", 'time': "10h 20m" },
      { 'activity': "Activity 5", 'time': "10h 20m" },
      { 'activity': "Activity 6", 'time': "10h 20m" },
      { 'activity': "Activity 7", 'time': "10h 20m" },
      { 'activity': "Activity 8", 'time': "10h 20m" },
      { 'activity': "Activity 9", 'time': "10h 20m" },



    ]
    var days = 0
    for (var i = 0; i < leaveArr.length; i++) {
      days = days + leaveArr[i].totalDays
    }
    // var customProjectListForModal = []
    // customProjectListForModal.push({ projectName: "All", select: false })
    // for (var i = 0; i < projectArr.length; i++) {
    //   customProjectListForModal.push({ projectName: projectArr[i].projectName, select: false })
    // }
    this.setState({ totalProjectList: projectArr, leaveList: leaveArr, totalLeaveDays: days, totalActivityList: activityArr, })

    for (var i = 0; i < 5; i++) {
      this.state.projectList.push(projectArr[i])
      this.state.activityList.push(activityArr[i])
    }

    this.setState({ projectList: this.state.projectList, activityList: this.state.activityList })
  }
  hitAllApi() {
    setTimeout(() => {
      this.setState({ spinnerVisible: false })
    }, 7000)
    AsyncStorage.getItem('userToken').then((token) => {

      this.setState({ userToken: token })
      this.hitGetAllLeaveTypesAPI(token)
      this.hitGetAllAnalysisDataAPI(token)
      //this.hitGetLeaveListAPI(token,"week")
      //getLeaveListAPI is called in makeWeekDataTabArray
      //this.makeWeekDateTabArray(token)
      this.hitGetAllProjectListApi(token)







    })
  }
  hitGetAllLeaveTypesAPI(token) {
    this.setState({ spinnerVisible: true })
    return webservice('', "leavetype/getlist", 'Post', token)
      .then(response => {
        this.setState({ spinnerVisible: false })
        if (response != "error") {
          if (response.leaveTypeList.length != 0) {
            var arr = []
            this.setState({
              leaveTypeListPassedToLeaveModal: response.leaveTypeList
            })


            // for (var i = 0; i < response.leaveTypeList.length; i++) {
            //   arr.push(response.leaveTypeList[i].leaveTypeName)
            // }
            // this.setState({ leaveTypeList: response.leaveTypeList, leaveTypeListPassToModal: arr })

          }
          getLog("Leave typeList=====++++" + JSON.stringify(response))



        }

      })
  }
  hitGetAllProjectListApi(token) {
    let variables = new FormData();
    this.setState({ spinnerVisible: true })
    variables.append("page", 0)

    return webservice(variables, "project/getlist", 'POST', token)
      .then(response => {
        this.setState({ spinnerVisible: false })
        if (response != "error") {
          if (response.projectList.length != 0) {
            var arr = []
            arr.push({ projectName: "All", select: false })
            for (var i = 0; i < response.projectList.length; i++) {
              arr.push({ projectName: response.projectList[i].projectName, select: false, projectId: response.projectList[i].projectId })
            }
            this.setState({ customProjectListForModal: arr, customProjectListForModalThatNotChanges: arr })

            // var arr = response.projectList
            // for (var i = 0; i < response.projectList.length; i++) {
            //   arr[i].select = false
            // }
            // this.setState({ projectListPassedToSelectProjectDropdown: arr, projectListContainAllProjectOfSelectProjectDropdown: arr })

          }

        }
        getLog("ProjectList=====++++" + JSON.stringify(response.projectList))
      })
  }
  hitGetAllAnalysisDataAPI(token) {
    let variables = new FormData();
    this.setState({ spinnerVisible: true })

    variables.append("startDate", new Date(this.state.startDate).getTime())
    variables.append("endDate", new Date(this.state.endDate).getTime())

    return webservice(variables, "analysis", 'POST', token)
      .then(response => {
        this.setState({ spinnerVisible: false })
        if (response != "error") {
          response.summaryData
          if (response.summaryData) {
            this.setState({
              projectTotalTimeSummary: response.summaryData.totalTime,
              projectDailyAvgHourSummary: response.summaryData.avgTime,
              projectTotalBillingSummary: response.summaryData.billing,
            })
          }
          if (response.projectData.length != 0) {

          }

        }
        getLog("analysis list=====++++" + JSON.stringify(response))
      })
  }

  onTabPress() {
    this.setState({ TabPress: !this.state.TabPress })
  }
  createList(item, index) {

    var progress = item.progress + "%"
    var left = (100 - item.progress) + "%"
    return (
      <TouchableWithoutFeedback  >
        <View style={styles.listView}>
          <View style={[styles.rowView, { padding: this.state.TabPress ? 5 : 10 }]}>
            {this.state.TabPress ?
              <View style={{ justifyContent: 'center', alignItems: "center", marginHorizontal: 5, marginRight: 10 }}>
                <View style={{ height: 16, width: 16, borderRadius: 8, backgroundColor: sliceColor[index] ? sliceColor[index] : "#6893FF", marginTop: -12, marginLeft: -5 }}>
                </View>
              </View>
              : null}
            <View style={[styles.projectColumnView, { flex: this.state.TabPress ? 3.5 : 1.5 }]}>
              <Text style={{ fontSize: fontSizes.font16, color: "#333333" }}>
                {this.state.TabPress ? item.projectName : item.type}
              </Text>
              {this.state.TabPress ?
                <Text style={{ fontSize: fontSizes.font14, color: "#BBBBBB", paddingTop: 5 }}>
                  {"$" + item.price}
                </Text>
                : null}

            </View>
            {!this.state.TabPress ? <View style={[styles.projectColumnView, { flex: 1.5, marginHorizontal: 5, flexDirection: "row", alignItems: "center" }]}>
              <View style={{ width: progress, backgroundColor: "#2A56C6", height: 2 }} />
              <View style={{ width: left, backgroundColor: "#D8D8D8", height: 2 }} />
            </View>
              : null}
            <View style={[styles.projectColumnView, { flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }]}>

              <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                {this.state.TabPress ? item.time : item.totalDays > 1 ? item.totalDays + " Days" : item.totalDays + " Day"}
              </Text>
              {!this.state.TabPress ?
                <Image source={require('../images/rightArrow.png')} /> : null
              }


            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  createActivityList(item, index) {
    return (
      <TouchableWithoutFeedback key={index + "row"} >
        <View style={[styles.listView, { marginHorizontal: 0 }]}>
          <View style={[styles.rowView, { padding: this.state.TabPress ? 5 : 10 }]}>
            <View style={[styles.projectColumnView, { flex: this.state.TabPress ? 3.5 : 1.5 }]}>
              <Text style={{ fontSize: fontSizes.font18, color: "#333333", marginLeft: 15 }}>
                {item.activity}
              </Text>
            </View>

            <View style={[styles.projectColumnView, { flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }]}>
              <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                {item.time}
              </Text>
            </View>
          </View>
        </View>

      </TouchableWithoutFeedback>
    )
  }
  createFlatList(listType) {

    return (
      <FlatList
        data={listType == 'activities' ? this.state.activityList : this.state.TabPress ? this.state.projectList : this.state.leaveList}
        renderItem={({ item, index }) => listType == 'activities' ? this.createActivityList(item, index) : this.createList(item, index)}
        keyExtractor={key => key.index}
        extraData={this.state}
      />
    )
  }
  showStartDatePicker() {
    this.setState({ startDateTimePickerVisible: true })
  }
  showEndDatePicker() {
    this.setState({ endDateTimePickerVisible: true })
  }
  _handleStartDatePicked(date) {
    this.setState({ startDateTimePickerVisible: false, startDate: date, endDate: date })
    setTimeout(() => {
      this.showEndDatePicker()
    }, 500)
    //alert(date)
  }
  _handleEndDatePicked(date) {
    this.setState({ endDateTimePickerVisible: false, endDate: date })
    setTimeout(() => {
      this.hitGetAllAnalysisDataAPI(this.state.userToken)
    }, 300)

    //var date = dateConverterUseInHeader(this.state.startDate) + '-' + dateConverterUseInHeader(date)
    //this.setState({ datePassedToHeader: date })
  }
  onFloatingButtonPressed() {
    //alert()
  }
  onProjectSelectDropdown() {
    var arr = this.state.customProjectListForModalThatNotChanges
    for (var i = 0; i < arr.length; i++) {
      arr[i].select = false
    }
    this.setState({ selectProjectModal: true, PickerDropdownModalFlatListData: arr })
  }
  onBillableSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, pickerDropdownModalTitleText: 'Select Activity Type', PickerDropdownModalFlatListData: billableData, type: "Billable" })
  }
  onLeaveSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, pickerDropdownModalTitleText: 'Leave Type', PickerDropdownModalFlatListData: this.state.leaveTypeListPassedToLeaveModal, type: "Leave Type" })
  }
  pickerDropdownModalRowSelected(item) {
    if (this.state.type == 'Select Project') {
      this.setState({ selectProject: item.projectName })
    } else if (this.state.type == 'Leave Type') {
      this.setState({ leaveType: item.type })
    }
    else {
      this.setState({ billableAmount: item })
    }

    this.setState({ pickerDropdownModalVisible: false })
  }
  onChangeOfSearchTextInputOfSelectProjectModal(text) {
    if (text != '') {
      var arr = []
      var typeArr = this.state.customProjectListForModalThatNotChanges
      for (var i = 1; i < typeArr.length; i++) {

        if (typeArr[i].projectName.toLowerCase().indexOf(text.toLowerCase()) != -1) {
          arr.push(typeArr[i])
        }
      }
      this.setState({ customProjectListForModal: arr })
    } else {
      var typeArr = this.state.customProjectListForModalThatNotChanges
      for (var i = 0; i < typeArr.length; i++) {

        if (typeArr[i].select) {
          typeArr[0].select = true



        } else {
          typeArr[0].select = false



          break
        }


      }
      this.setState({ customProjectListForModal: typeArr })
    }



  }
  customProjectListForModalRowSelected(item, index) {
    //alert(JSON.stringify(item))
    var arr = this.state.customProjectListForModal
    var totalArr = this.state.customProjectListForModalThatNotChanges
    if (item.projectName == "All") {
      for (var i = 1; i < arr.length; i++) {
        if (arr[0].select) {
          arr[i].select = false
        } else {
          arr[i].select = true
        }

      }
      arr[0].select = !arr[0].select
      //this.setState({ customProjectListForModal: arr,customProjectListForModalThatNotChanges: totalArr })
    } else {

      arr[index].select = !arr[index].select
      //  var totalIndex = totalArr.findIndex(obj => obj.projectId == item.projectId);
      //  totalArr[totalIndex].select = !totalArr[totalIndex].select
      for (var i = 0; i < arr.length; i++) {
        if (arr[i].projectName != "All") {
          if (arr[i].select) {
            totalArr[0].select = true

            arr[i].select = true

          } else {
            totalArr[0].select = false

            arr[i].select = false

            break
          }
        }


      }
      //alert(customProjectListForModal[index].select)
      //alert(arr[index].select+"=="+totalIndex)
      //this.setState({ customProjectListForModal: arr,customProjectListForModalThatNotChanges: totalArr  })
    }
    setTimeout(() => {
      this.setState({ customProjectListForModal: arr, customProjectListForModalThatNotChanges: totalArr })
    }, 50)

    //alert(JSON.stringify(this.state.customProjectListForModal[index]))
  }
  pickerDropdownSelectClickedOfSelectProject() {
    var typeArr = this.state.customProjectListForModalThatNotChanges
    for (var i = 0; i < typeArr.length; i++) {

      if (typeArr[i].select) {
        typeArr[0].select = true



      } else {
        typeArr[0].select = false



        break
      }


    }
    this.setState({ selectProjectModal: false })
    // here the above loops checks that if any of item is select=false then it assign the all value to false bcz
    // when we search in text field and select all the data then according to condition the all value is set true
    // so we have to check every time means on backspace to text=='' this loops fires also if user select "Select" 
    //option when only 3 data in list by using search and all are selected then all select are true 
    //so this loop executes and set it to false.
  }
  viewMoreActivity() {
    //alert()
    var count = 0
    var arr = this.state.activityList

    if (this.state.activityList.length >= this.state.totalActivityList.length) {
      this.setState({ activityViewMoreVisible: false })
    } else {

      for (var i = this.state.activityList.length; i <= this.state.totalActivityList.length - 1; i++) {
        if (count < 5) {
          arr.push(this.state.totalActivityList[i]);
          count++
        }
      }
      this.setState({ activityList: arr, showActivity: true })
    }


  }
  viewMoreProject() {
    var count = 0
    var arr = this.state.projectList

    if (this.state.projectList.length >= this.state.totalProjectList.length) {
      this.setState({ projectViewMoreVisible: false })
    } else {

      for (var i = this.state.projectList.length; i <= this.state.totalProjectList.length - 1; i++) {
        if (count < 5) {
          arr.push(this.state.totalProjectList[i]);
          count++
        }
      }
      this.setState({ projectList: arr, showProjects: true })
    }
  }




  render() {
    return (
      <SafeAreaView
        style={styles.safeArea}>
        <View style={styles.container}>
          <Spinner visible={this.state.spinnerVisible} />
          <Header HeaderLeftText="Analysis"
            HeaderRightText={dateConverterUseInHeader(this.state.startDate) + '-' + dateConverterUseInHeader(this.state.endDate)}
            type="date"
            toggleDrawer={() => this.props.navigation.openDrawer()}
            //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
            headerRightIcon={require('../images/analysisExportIcons/dateIcon.png')}
            headerRightIconOnPress={() => this.showStartDatePicker()} />
          <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatDownloadIcon.png')} />



          <PickerDropdownModal
            pickerDropdownModalVisible={this.state.pickerDropdownModalVisible}
            pickerDropdownModalClose={() => this.setState({ pickerDropdownModalVisible: false })}
            titleText={this.state.pickerDropdownModalTitleText}
            pickerDropdownModalListData={this.state.PickerDropdownModalFlatListData}
            pickerDropdownModalRow={({ item, index }) =>
              <TouchableWithoutFeedback key={"abc" + index}
                onPress={() => this.pickerDropdownModalRowSelected(item)}>
                <View style={{ padding: 15, paddingLeft: 25, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                  <Text style={{ fontSize: fontSizes.font18, color: "#333333" }}>
                    {item.projectName ? item.projectName : item.leaveTypeName ? item.leaveTypeName : item}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            }
          />

          <SelectProjectModal
            pickerDropdownModalVisible={this.state.selectProjectModal}
            pickerDropdownModalClose={() => this.setState({ selectProjectModal: false, customProjectListForModal: this.state.customProjectListForModalThatNotChanges })}
            titleText="Select Project"
            pickerDropdownModalListData={this.state.customProjectListForModal}
            extraData={this.state}
            type='multiSelect'
            onChangeOfSearchTextInput={(text) => this.onChangeOfSearchTextInputOfSelectProjectModal(text)}
            pickerDropdownSelectClicked={() => this.pickerDropdownSelectClickedOfSelectProject()}
            pickerDropdownModalRow={({ item, index }) =>
              <TouchableWithoutFeedback key={"abc" + index}
                onPress={() => this.customProjectListForModalRowSelected(item, index)}>
                <View style={{ padding: 12, paddingLeft: 25, alignItems: "center", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: index == this.state.customProjectListForModal.length - 1 ? 0 : 1, flexDirection: "row" }}>
                  <Image source={item.select == true ? require('../images/createTimesheet/tickIcon.png') : require('../images/createTimesheet/uncheckIcon.png')} />
                  <Text style={{ fontSize: fontSizes.font16, color: "#333333", marginLeft: 15 }}>
                    {item.projectName}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            }
          />

          <DateTimePicker
            isVisible={this.state.startDateTimePickerVisible}
            onConfirm={(date) => this._handleStartDatePicked(date)}
            mode='date'
            titleIOS="Pick Start Date"
            onCancel={() => this.setState({ startDateTimePickerVisible: false })}
          />
          <DateTimePicker
            isVisible={this.state.endDateTimePickerVisible}
            onConfirm={(date) => this._handleEndDatePicked(date)}
            mode='date'
            minimumDate={new Date(this.state.startDate)}
            titleIOS="Pick End Date"
            onCancel={() => this.setState({ endDateTimePickerVisible: false })}
          />
          <View style={styles.mainContainer}>
            {/* {this.timesheet_LeavesTab()} */}
            <TimeLeaveTab onTabPress={() => this.onTabPress()} tabPress={this.state.TabPress} />

            <ScrollView showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="always">
              <View style={[styles.projectListContainer, { marginLeft: 0, marginRight: 0, paddingBottom: 50 }]}>
                {this.state.TabPress ?
                  <View style={[styles.dropdownRowView]}>
                    <TouchableWithoutFeedback onPress={() => this.onProjectSelectDropdown()}>
                      <View style={styles.leftDropdownStyle}>
                        <Text style={[styles.ProjectHeaderText, { color: 'black' }]}>
                          {this.state.selectProject}
                        </Text>
                        <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />
                      </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this.onBillableSelectDropdown()}>
                      <View style={[styles.rightDropdownStyle, { marginRight: 0 }]}>
                        <Text style={[styles.ProjectHeaderText, { color: 'black' }]}>
                          {this.state.billableAmount}
                        </Text>
                        <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                  :
                  <View style={styles.dropdownRowView}>
                    <View style={{ flex: 1 }} />
                    <TouchableWithoutFeedback onPress={() => this.onLeaveSelectDropdown()}>
                      <View style={[styles.rightDropdownStyle, { justifyContent: 'flex-end', marginRight: 0 }]}>
                        <Text style={[styles.ProjectHeaderText, { color: 'black', textAlign: "right", marginRight: 10 }]}>
                          {this.state.leaveType}
                        </Text>
                        <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                }

                {/* Summary Card Code */}
                {/* <CardView
                style={{ backgroundColor: 'transparent', marginTop: 10, paddingBottom: 10, paddingTop: 5 }}
                cardElevation={2}

                cardMaxElevation={2}
                cornerRadius={2}> */}

                <View style={[styles.summaryContainer, { paddingLeft: 0, paddingRight: 0 }]}>
                  <TouchableWithoutFeedback onPress={() => this.setState({ showSummary: !this.state.showSummary })}>
                    <View style={[styles.summaryColumnView, { borderBottomWidth: this.state.showSummary ? 1 : 0, paddingLeft: 0 }]}>
                      <View style={styles.summaryRowView}>
                        <Image style={{ marginTop: 4 }} source={this.state.showSummary ? require('../images/analysisExportIcons/arrowUp.png') : require('../images/analysisExportIcons/downArrow.png')} />
                        <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left", marginLeft: 10 }]}>
                          Summary (Totals)
                      </Text>

                      </View>
                      {!this.state.TabPress ?
                        <Text style={styles.totalnotChangeText}>
                          Total will not change with date filter
                    </Text> : <Text style={[styles.totalnotChangeText, { margin: 0 }]}>

                        </Text>
                      }
                    </View>
                  </TouchableWithoutFeedback>
                  {this.state.showSummary ?
                    <View style={[styles.rowView, { padding: 10 }]}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View
                          style={{ flex: 1, alignItems: "center" }}>

                          <Text style={[styles.ProjectHeaderText, { fontSize: fontSizes.font20, marginTop: 5 }]}>
                            {this.state.TabPress ? hourMinuteConverter(this.state.projectTotalTimeSummary) : this.state.leaveTotalAccuredSummary}
                          </Text>
                          <Text style={{ color: "#333333", textAlign: "center" }}>
                            {this.state.TabPress ? 'Time' : 'Accrued'}
                          </Text>
                        </View>

                        <View
                          style={{ flex: 1, alignItems: "center" }}>

                          <Text style={[styles.ProjectHeaderText, { fontSize: fontSizes.font20, marginTop: 5 }]}>
                            {this.state.TabPress ? hourMinuteConverter(this.state.projectDailyAvgHourSummary) : this.state.leaveTotalBalanceSummary}

                          </Text>
                          <Text style={{ color: "#333333", textAlign: "center", }}>
                            {this.state.TabPress ? "Daily Avg. Hrs" : "Balance"}
                          </Text>
                        </View>
                        <View
                          style={{ flex: 1, alignItems: "center" }}>

                          <Text style={[styles.ProjectHeaderText, { fontSize: fontSizes.font20, marginTop: 5 }]}>
                            {this.state.TabPress ? this.state.projectTotalBillingSummary : this.state.leaveTotalAvailedSummary}

                          </Text>
                          <Text style={{ color: "#333333", textAlign: "center", }}>
                            {this.state.TabPress ? "Billing ($)" : "Availed"}
                          </Text>
                        </View>
                      </View>
                    </View>
                    : null}
                </View>

                {/* </CardView> */}
                {this.state.TabPress ?
                  <View style={{ padding: 20, alignItems: 'center', justifyContent: "center", width: width }}>
                    <PieChart
                      chart_wh={chart_wh}
                      series={series}
                      sliceColor={sliceColor}
                      doughnut={true}
                      coverRadius={0.55}
                      coverFill={'#FFF'}
                    />
                  </View>
                  : null}

                {/* bar graph code */}
                {this.state.TabPress ?
                  <View
                    style={{ backgroundColor: 'transparent', marginTop: 10, paddingBottom: 10, paddingRight: 15, borderWidth: 0, borderColor: "#E5E5E588", borderRadius: 6, }}
                  >
                    <View style={[styles.headerView, { borderBottomWidth: this.state.showBarGraph ? .5 : 0, marginHorizontal: 5 }]}>
                      <TouchableWithoutFeedback onPress={() => this.setState({ showBarGraph: !this.state.showBarGraph })}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 10, paddingBottom: 10 }}>
                          <View style={[styles.summaryRowView, { flex: 1 }]}>

                            <View >
                              <Image style={{ marginTop: 4 }} source={this.state.showBarGraph ? require('../images/analysisExportIcons/arrowUp.png') : require('../images/analysisExportIcons/downArrow.png')} />
                            </View>

                            <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left", marginLeft: 10 }]}>
                              Avg. Daily Time (Hrs)
                            </Text>

                          </View>
                        </View>
                      </TouchableWithoutFeedback>
                    </View>


                    {this.state.showBarGraph ?
                      <BarGraph />
                      : null}







                  </View>
                  : null}
                {/* Project Card Code */}
                <View
                  style={{ backgroundColor: 'transparent', marginTop: 10, paddingBottom: 10, borderWidth: 0, borderColor: "#E5E5E588", borderRadius: 6 }}
                >
                  {/* <View style={{height:180,marginHorizontal:40,backgroundColor:"red"}}>
                <VictoryPie
                    colorScale={["tomato", "orange", "gold", "cyan", "navy"]}
                    innerRadius={30}
                    height={width-80}
                    width={width-80}
                    data={[
                      { x: "Cats", y: 35 },
                      { x: "Dogs", y: 40 },
                      { x: "Birds", y: 55 }
                    ]}
                  />
                </View> */}

                  <View style={[styles.headerView, { borderBottomWidth: this.state.showProjects || !this.state.TabPress ? 1 : 0, marginHorizontal: 5 }]}>
                    <TouchableWithoutFeedback onPress={() => this.setState({ showProjects: !this.state.showProjects })}>
                      <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 10, paddingBottom: 10 }}>
                        <View style={[styles.summaryRowView, { flex: 1 }]}>
                          {this.state.TabPress ?
                            <View >
                              <Image style={{ marginTop: 4 }} source={this.state.showProjects ? require('../images/analysisExportIcons/arrowUp.png') : require('../images/analysisExportIcons/downArrow.png')} />
                            </View>
                            : null}
                          <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left", marginLeft: 10 }]}>
                            {this.state.TabPress ? "Projects" : "Type"}
                          </Text>

                        </View>
                        {/* <Text style={styles.ProjectHeaderText}>
                      {this.state.TabPress ? "Projects" : "Type"}
                    </Text> */}
                        <Text style={[styles.ProjectHeaderTime, { marginRight: 15 }]}>

                          {this.state.TabPress ? "40h 40m" : this.state.totalLeaveDays > 1 ? this.state.totalLeaveDays + " Days" : this.state.totalLeaveDays + " Day"}

                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                  {this.state.TabPress ? this.state.showProjects ? this.createFlatList("projectLeave") : null : this.createFlatList("projectLeave")}
                  {
                    this.state.TabPress ?
                      this.state.showProjects ?
                        <View style={[styles.viewMoreView, {}]}>
                          <TouchableOpacity onPress={() => this.viewMoreProject()}>
                            <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "right", marginRight: 20 }]}>
                              {this.state.projectViewMoreVisible ? 'View more..' : ''}
                            </Text>
                          </TouchableOpacity>
                        </View>
                        : null : null

                  }
                </View>
                {/* Activities Card Code */}

                {this.state.TabPress ?
                  <View
                    style={{ backgroundColor: 'transparent', marginTop: 10, paddingBottom: 10, paddingRight: 15, borderWidth: 0, borderColor: "#E5E5E588", borderRadius: 6 }}
                  >
                    <View style={[styles.headerView, { borderBottomWidth: this.state.showActivity ? .5 : 0, marginHorizontal: 5 }]}>
                      <TouchableWithoutFeedback onPress={() => this.setState({ showActivity: !this.state.showActivity })}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 10, paddingBottom: 10 }}>
                          <View style={[styles.summaryRowView, { flex: 1 }]}>

                            <View >
                              <Image style={{ marginTop: 4 }} source={this.state.showActivity ? require('../images/analysisExportIcons/arrowUp.png') : require('../images/analysisExportIcons/downArrow.png')} />
                            </View>

                            <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left", marginLeft: 10 }]}>
                              Activities
                      </Text>

                          </View>
                          {/* <Text style={styles.ProjectHeaderText}>
                      {this.state.TabPress ? "Projects" : "Type"}
                    </Text> */}
                          <Text style={styles.ProjectHeaderTime}>

                            40h 40m

                        </Text>
                        </View>
                      </TouchableWithoutFeedback>
                    </View>
                    {
                      this.state.showActivity ? this.createFlatList("activities") : null
                    }
                    {
                      this.state.showActivity ?
                        <View style={[styles.viewMoreView, {}]}>
                          <TouchableOpacity onPress={() => this.viewMoreActivity()}>
                            <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "right", marginRight: 20 }]}>
                              {this.state.activityViewMoreVisible ? 'View more..' : ''}
                            </Text>
                          </TouchableOpacity>
                        </View>
                        : null
                    }


                  </View>
                  : null}


                {/* Bar Graph View */}
                {/* {this.state.TabPress ?
                  <View
                    style={{ backgroundColor: 'transparent', marginTop: 10, paddingBottom: 10, paddingRight: 15, borderWidth: 0, borderColor: "#E5E5E588", borderRadius: 6, marginBottom: 80 }}
                  >
                    <View style={[styles.headerView, { borderBottomWidth: this.state.showBarGraph ? .5 : 0, marginHorizontal: 5 }]}>
                      <TouchableWithoutFeedback onPress={() => this.setState({ showBarGraph: !this.state.showBarGraph })}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 10, paddingBottom: 10 }}>
                          <View style={[styles.summaryRowView, { flex: 1 }]}>

                            <View >
                              <Image style={{ marginTop: 4 }} source={this.state.showBarGraph ? require('../images/analysisExportIcons/arrowUp.png') : require('../images/analysisExportIcons/downArrow.png')} />
                            </View>

                            <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left", marginLeft: 10 }]}>
                              Avg. Daily Time (Hrs)
                            </Text>

                          </View>
                        </View>
                      </TouchableWithoutFeedback>
                    </View>


                    {this.state.showBarGraph ?
                      <BarGraph />
                      : null}







                  </View>
                  : null} */}
              </View>
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#21459E'
  },
  container: { flex: 1, backgroundColor: '#fff', },
  mainContainer: { flex: 1, justifyContent: "center", alignItems: 'center', },
  projectListContainer: { margin: 10, width: width, },
  ProjectHeaderText: { fontSize: fontSizes.font18, color: "#2A56C6", textAlign: 'center' },
  ProjectHeaderTime: { fontSize: fontSizes.font16, color: "#FF9800", textAlign: 'center' },
  headerView: { padding: 10, borderBottomColor: '#E5E5E5', marginHorizontal: 10 },
  listView: { borderBottomWidth: .5, padding: 10, paddingTop: 5, paddingBottom: 5, borderBottomColor: '#E5E5E5', marginHorizontal: 10 },
  rowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
  summaryColumnView: { paddingBottom: 5, padding: 5, borderBottomColor: "#E5E5E5" },
  summaryRowView: { flexDirection: "row", alignItems: "center", },
  projectColumnView: { flex: 1.5, justifyContent: "center", },
  //TimerContainer: { height: 100, width: width - 20, borderRadius: 6, borderWidth: .5, borderColor: "white", shadowOffset: { width: 0, height: 3, }, shadowColor: '#000', shadowOpacity: 0.8, shadowRadius: 5 }
  summaryContainer: { width: width - 20, borderRadius: 6, borderWidth: 0, borderColor: "#E5E5E588", padding: 10, marginTop: 10, paddingBottom: 10, paddingTop: 5, paddingRight: 15, paddingLeft: 12 },
  dropdownRowView: { flexDirection: "row", width: width - 20, marginHorizontal: 10 },
  leftDropdownStyle: { flex: 1, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, flexDirection: "row", justifyContent: "space-between", padding: 10, marginRight: 10 },
  rightDropdownStyle: { flex: 1, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, flexDirection: "row", justifyContent: "space-between", padding: 10, marginLeft: 10, marginRight: 20 },
  totalnotChangeText: { flex: 1, textAlign: "left", marginLeft: 25, fontSize: fontSizes.font12, color: "#757575", margin: 5 },
  viewMoreView: { width: width - 20, marginTop: 10, }


});
