/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, SafeAreaView, PanResponder,
    Text, Keyboard, AsyncStorage,
    View, Image, ScrollView, TouchableOpacity, FlatList
} from 'react-native';

import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import ProjectModal from '../components/projectModal'
import Spinner from '../components/spinner'
import LeaveModal from '../components/leaveModal'
import { height, width, fontSizes, getLog } from '../utils/utils'
import { dateConverter, dateConverterUseInHeader } from '../components/dateConverter'
import webservice from '../components/webService'
var keyboardOpen = false
var projectListArr = []
export default class analysisExport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabId: "Projects",
            flatListdata: [],
            projectList: [],
            activitiList: [],
            leaveTypeList: [],
            projectModalVisible: false,
            projectNameModalText: '',
            projectCodeModalText: '',
            projectBillingModalText: 0,
            projectModalDatePickerVisible: false,
            datePickerType: "",
            startDateToShowOnMOdal: '',
            endDateToShowOnModal: '',
            startDate: '',
            endDate: '',
            projectModalTitleText: "New Project",
            leaveModalTitleText: 'Add Leave',
            editedIndex: null,
            projectCurrencyModalText: "Currency",
            projectNameMandatory: false,
            projectNameMandatoryText: '',
            projectId: '',
            //leave modal states

            leaveModalVisible: false,
            totalDays: 0,
            reasonTextInputData: '',
            leaveTypeOnLeaveModal: "Leave Type",
            leaveTypeListPassOnModal: [],
            leaveBalance: '',
            leaveTypeTextInputDataOnModal: '',
            minDate: '',
            userToken: '',
            spinnerVisible: false,
            leaveModalErrorText: '',
            leaveTypeId: ''
        }
    }
    hitAllAPI() {
        setTimeout(() => {
            this.setState({ spinnerVisible: false })
        }, 7000)
        AsyncStorage.getItem('userToken').then((token) => {
            this.setState({ userToken: token })
            this.hitGetAllProjectListApi(token)
            this.hitGetAllActivitiesAPI(token)
            this.hitGetAllLeaveTypesAPI(token)






        })
    }
    hitGetAllProjectListApi(token) {
        let variables = new FormData();
        //this.setState({ spinnerVisible: true, projectNameMandatory: false })
        variables.append("page", 0)

        return webservice(variables, "project/getlist", 'POST', token)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {
                    if (response.projectList.length != 0) {
                        this.setState({ spinnerVisible: true })
                        var arr = []
                        for (var i = 0; i < response.projectList.length; i++) {
                            arr.push({
                                name: response.projectList[i].projectName,
                                id: response.projectList[i].projectCode,
                                costPerHour: response.projectList[i].billing == 0 ? '' : response.projectList[i].billing,
                                currency: response.projectList[i].currency,
                                validityFrom: response.projectList[i].startDate,
                                validityTo: response.projectList[i].endDate,
                                projectId: response.projectList[i].projectId
                            })
                        }

                        this.setState({ spinnerVisible: false, projectList: arr })
                        if (this.props.id == 'manageProject') {
                            this.setState({ flatListdata: arr, tabId: "Projects" })
                        }


                        // projectList
                        // projectListArr=arr

                    }
                    getLog("ProjectList=====++++" + JSON.stringify(arr))



                } else {
                    getLog("ProjectList err response=====++++" + JSON.stringify(response))
                    this.setState({ spinnerVisible: false })
                }

            })
    }
    hitGetAllActivitiesAPI(token) {
        return webservice('', "activity/getlist", 'Post', token)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {
                    if (response.activityList.length != 0) {

                        var arr = []
                        for (var i = 0; i < response.activityList.length; i++) {
                            arr.push({
                                name: response.activityList[i].activityName,
                                type: response.activityList[i].activityId
                            })
                        }
                        this.setState({ spinnerVisible: false, activitiList: arr })
                        if (this.props.id == 'activityList') {
                            this.setState({ flatListdata: arr, tabId: "Activities" })
                        }
                    }
                    getLog("ActivityList=====++++" + JSON.stringify(response))



                }

            })
    }
    hitGetAllLeaveTypesAPI(token) {
        return webservice('', "leavetype/getlist", 'Post', token)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {
                    if (response.leaveTypeList.length != 0) {

                        var arr = []
                        var leaveTypeArr = []
                        for (var i = 0; i < response.leaveTypeList.length; i++) {
                            arr.push({
                                name: response.leaveTypeList[i].leaveTypeName,
                                leaveCount: response.leaveTypeList[i].balance,
                                validityFrom: response.leaveTypeList[i].startDate,
                                validityTo: response.leaveTypeList[i].endDate,
                                leaveTypeId: response.leaveTypeList[i].leaveTypeId
                            })
                            leaveTypeArr.push(response.leaveTypeList[i].leaveTypeName)
                        }



                        this.setState({ spinnerVisible: false, leaveTypeList: arr, leaveTypeListPassOnModal: leaveTypeArr })
                        if (this.props.id == 'manageLeaveTypes') {
                            this.setState({ flatListdata: arr, tabId: "Leave Types" })
                        }
                    }
                    getLog("Leave typeList=====++++" + JSON.stringify(response))



                }

            })
    }
    componentWillMount() {
        this.setState({ spinnerVisible: true })

        this.hitAllAPI()
        // var projectListArr = [
        //     { name: "Project XYZ", id: "AAFGT#$%^", costPerHour: '14', currency: "$", validityFrom: new Date(), validityTo: new Date() },
        //     { name: "Project XYZ", id: "", costPerHour: '14', currency: "$", validityFrom: new Date(), validityTo: new Date() },
        //     { name: "Project XYZ", costPerHour: '14', currency: "$", validityFrom: new Date(), validityTo: new Date() },
        //     { name: "Project XYZ", id: "", costPerHour: '', currency: "", validityFrom: '', validityTo: '' },

        //     // { name: "Project XYZ", id: "AAFGT#$%^", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
        //     // { name: "Project XYZ", id: "", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
        //     // { name: "Project XYZ", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
        //     // { name: "Project XYZ", id: "", costPerHour: '', currency: "", validityFrom: "", validityTo: "" },
        //     // { name: "Project XYZ", id: "AAFGT#$%^", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
        //     // { name: "Project XYZ", id: "", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
        //     // { name: "Project XYZ", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
        //     // { name: "Project XYZ", id: "", costPerHour: '', currency: "", validityFrom: "", validityTo: "" },
        // ]

        // var activityListArr = [
        //     { name: "Metting", type: "activity" },
        //     { name: "Development", type: "activity" },
        //     { name: "Testing", type: "activity" },
        //     { name: "Deployment", type: "activity" },

        //     { name: "Metting", type: "activity" },
        //     { name: "Development", type: "activity" },
        //     { name: "Testing", type: "activity" },
        //     { name: "Deployment", type: "activity" },
        //     { name: "Metting", type: "activity" },
        //     { name: "Development", type: "activity" },
        //     { name: "Testing", type: "activity" },
        //     { name: "Deployment", type: "activity" },
        // ]

        // var leaveListArr = [
        //     { name: "Planned(PL)", leaveCount: 12, validityFrom: new Date(), validityTo: new Date() },
        //     { name: "Sick(PL)", leaveCount: 12, validityFrom: new Date(), validityTo: new Date() },
        //     { name: "Casual(CL)", leaveCount: 12, validityFrom: new Date(), validityTo: new Date() },
        //     { name: "Project XYZ", leaveCount: 12, validityFrom: new Date(), validityTo: new Date() },

        // ]


        // setTimeout(() => {
        //     this.setState({ flatListdata: this.state.projectList })
        // }, 300)
        if (this.props.id) {
            //alert(this.props.id)
            if (this.props.id == 'activityList') {
                this.setState({ flatListdata: this.state.activitiList, tabId: "Activities" })
            }
            if (this.props.id == 'manageLeaveTypes') {
                this.setState({ flatListdata: this.state.leaveTypeList, tabId: "Leave Types" })
            }
            if (this.props.id == 'manageProject') {
                this.setState({ flatListdata: this.state.projectList, tabId: "Projects" })
            }
        }


        AsyncStorage.getItem('userToken').then((token) => {
            this.setState({ userToken: token })

        })







        this.wrapperPanResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderEnd: (e, { vx, dx, dy }) => {

                console.log("end" + dy)
                if (dy > -10 && dy < 10) {


                    if (dx < 0) {
                        //this.setState({ globalLeft: "right" })
                        if (this.state.tabId == "Projects") {
                            this.setState({ tabId: "Activities" })
                            this.setState({ flatListdata: this.state.activitiList })
                        } else if (this.state.tabId == "Activities") {
                            this.setState({ tabId: "Leave Types" })
                            this.setState({ flatListdata: this.state.leaveTypeList })
                        } else {

                        }
                        console.log("right")

                    } else {
                        //this.setState({ globalLeft: "left" })
                        if (this.state.tabId == "Leave Types") {
                            this.setState({ tabId: "Activities" })
                            this.setState({ flatListdata: this.state.activitiList })
                        } else if (this.state.tabId == "Activities") {
                            this.setState({ tabId: "Projects" })
                            this.setState({ flatListdata: this.state.projectList })
                        } else {

                        }

                        console.log("left")
                    }
                }
                return true;
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
                return true;
            },
        });


    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardOpen);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardClose);
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    static navigationOptions = {
        drawerLabel: 'Manage Projects',
        drawerIcon: ({ tintColor }) => (
            <Image source={require('../images/drawerIcons/manageProjectIcon.png')} />
        ),
    };
    keyboardOpen() {
        keyboardOpen = true
        //this.setState({ keyboardOpen: true })
    }

    keyboardClose() {
        keyboardOpen = false
        //this.setState({ keyboardOpen: false })
    }

    filterFunction() {

    }
    onFloatingButtonPressed() {
        getLog("tab id on floating button pressed" + this.state.tabId)
        if (this.state.tabId == "Projects") {
            this.setState({
                projectModalTitleText: "New Project",
                projectNameModalText: '',
                projectCodeModalText: '',
                startDateToShowOnMOdal: '',
                endDateToShowOnModal: '',
                projectBillingModalText: '',
                projectCurrencyModalText: 'Currency',
                projectNameMandatory: false,
                projectNameMandatoryText: ''

            })
            setTimeout(() => {
                this.setState({ projectModalVisible: true })
            }, 300)
        }

        if (this.state.tabId == "Leave Types") {
            this.setState({
                leaveModalTitleText: "Add Leave Type",
                leaveTypeOnLeaveModal: 'Leave Type',
                leaveTypeTextInputDataOnModal: '',
                leaveBalance: "",
                startDateToShowOnMOdal: '',
                endDateToShowOnModal: '',
                leaveModalErrorText: ''

            })
            setTimeout(() => {
                this.setState({ leaveModalVisible: true })
            }, 300)

        }
    }
    onTabPress(tabId) {
        this.setState({ tabId: tabId })
        if (tabId == 'Projects') {
            this.setState({ flatListdata: this.state.projectList })
        }
        if (tabId == 'Activities') {
            this.setState({ flatListdata: this.state.activitiList })
        }
        if (tabId == 'Leave Types') {
            this.setState({ flatListdata: this.state.leaveTypeList })
        }
    }
    makeTabs() {
        return (
            <View style={styles.tabView}>
                {/* first tab */}
                <TouchableOpacity onPress={() => this.onTabPress("Projects")}
                    style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.tabId == "Projects" ? 2 : 0 }]} >
                    <Text style={styles.tabText}>
                        Projects
                </Text>
                </TouchableOpacity>
                {/* second tab */}
                <TouchableOpacity onPress={() => this.onTabPress("Activities")}
                    style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.tabId == "Activities" ? 2 : 0 }]}   >
                    <Text style={styles.tabText}>
                        Activities
                </Text>
                </TouchableOpacity>
                {/* third tab */}
                <TouchableOpacity onPress={() => this.onTabPress("Leave Types")}
                    style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.tabId == "Leave Types" ? 2 : 0 }]}   >
                    <Text style={styles.tabText}>
                        Leave Types
                </Text>
                </TouchableOpacity>
            </View>
        )
    }
    //
    createList(item, index) {

        return (
            <View key={index + "View"} style={styles.listRowView}>
                <View style={{ flex: 6, marginLeft: 10 }}>
                    <Text style={styles.projectNameText}>
                        {item.name}
                    </Text>
                    {item.id ? item.id != '' ?
                        <Text style={styles.otherText}>
                            {item.id}
                        </Text>
                        : null : null}
                    {item.leaveCount ? item.leaveCount != '' ?
                        <Text style={styles.otherText}>
                            {item.leaveCount + " Leaves"}
                        </Text>
                        : null : null}
                    {item.costPerHour ? item.costPerHour != '' ?
                        <Text style={styles.otherText}>
                            {item.costPerHour + item.currency + " per hour"}
                        </Text>
                        : null : null}
                    {item.validityFrom ? (item.validityFrom != '' && item.validityTo != '') ?
                        <Text style={styles.otherText}>
                            {"validity: " + (item.validityFrom != '' ? dateConverterUseInHeader(new Date(item.validityFrom)) : '')
                                + " - " + (item.validityTo != '' ? dateConverterUseInHeader(new Date(item.validityTo)) : '')}
                        </Text>
                        : null : null}
                </View>
                {item.type ? null :
                    <TouchableOpacity onPress={() => this.onEditPress(item, index)}
                        style={{ flex: 1, alignItems: "center" }}>
                        <Image source={require('../images/settingIcons/editIcon.png')} style={{ marginTop: 5 }} />
                    </TouchableOpacity>
                }

            </View>
        )
    }


    startDateVisibleFunction() {
        this.setState({ projectModalDatePickerVisible: true, datePickerType: 'start', startDate: '' })
    }
    endDateVisibleFunction() {
        if (this.state.startDate == '') {
            alert("Please select start date first")
        } else {
            this.setState({ projectModalDatePickerVisible: true, datePickerType: 'end', minDate: this.state.startDate })
        }

    }
    getDateFromModal(date) {
        //alert(date)startDateToShowOnMOdal
        //this.setState({ minDate: date })
        if (this.state.datePickerType == 'start') {
            this.setState({ startDate: date, datePickerType: '', projectModalDatePickerVisible: false, endDateToShowOnModal: '', endDate: '' })
            var d = dateConverter(date)
            setTimeout(() => {
                this.setState({ startDateToShowOnMOdal: d, })
            }, 200)
        } if (this.state.datePickerType == 'end') {

            if (date < new Date(this.state.startDate)) {


                alert("End date should be greater than start sate")


            } else {
                this.setState({ endDate: date, datePickerType: '', projectModalDatePickerVisible: false, minDate: '' })
                var d = dateConverter(date)
                setTimeout(() => {
                    this.setState({ endDateToShowOnModal: d })
                }, 200)
            }

        }
    }
    onEditPress(item, index) {
        getLog("on edit pressed" + index + this.state.tabId)
        getLog("on edit pressed item" + JSON.stringify(item))
        if (this.state.tabId == "Projects") {
            this.setState({
                projectModalTitleText: "Edit Project",
                projectNameModalText: item.name,
                projectCodeModalText: item.id,
                startDateToShowOnMOdal: item.validityFrom != '' ? dateConverterUseInHeader(new Date(item.validityFrom)) : "Start Date",
                endDateToShowOnModal: item.validityTo != '' ? dateConverterUseInHeader(new Date(item.validityTo)) : "End Date",
                startDate: item.validityFrom,
                editedIndex: index,
                endDate: item.validityTo,
                projectBillingModalText: item.costPerHour,
                projectCurrencyModalText: item.currency == '' ? "Currency" : item.currency,
                projectId: item.projectId,
                projectNameMandatory: false,
                projectNameMandatoryText: ''

            })
            setTimeout(() => {
                this.setState({ projectModalVisible: true })
            }, 300)
        }

        if (this.state.tabId == "Leave Types") {
            this.setState({
                leaveModalTitleText: "Edit Leave Type",
                //leaveTypeOnLeaveModal: item.name,
                leaveTypeTextInputDataOnModal: item.name,
                leaveBalance: "" + item.leaveCount,
                editedIndex: index,
                leaveTypeId: item.leaveTypeId,
                startDateToShowOnMOdal: item.validityFrom != '' ? dateConverterUseInHeader(new Date(item.validityFrom)) : "Start Date",
                startDate: item.validityFrom,
                endDate: item.validityTo,
                endDateToShowOnModal: item.validityTo != '' ? dateConverterUseInHeader(new Date(item.validityTo)) : "End Date",
                leaveModalErrorText: '',

            })
            setTimeout(() => {
                this.setState({ leaveModalVisible: true })
            }, 300)

        }





    }
    closeProjectModal() {
        if (keyboardOpen) {
            Keyboard.dismiss()
        } else {
            this.setState({ projectModalVisible: false, minDate: '', startDate: '' })
        }
        //
    }
    closeLeaveModal() {
        if (keyboardOpen) {
            Keyboard.dismiss()
        } else {
            this.setState({ leaveModalVisible: false, minDate: '', startDate: '' })
        }
        //
    }

    onLeaveModalselectLeaveTypeClicked(index, value) {
        this.setState({ leaveTypeOnLeaveModal: value })
    }

    editProjectAPI() {
        var arr = this.state.projectList
        let variables = new FormData();
        this.setState({ spinnerVisible: true, projectNameMandatory: false, projectNameMandatoryText: '' })
        variables.append("projectName", this.state.projectNameModalText)
        variables.append("projectCode", this.state.projectCodeModalText)
        variables.append("projectId", this.state.projectId)
        if (this.state.startDate != '') {
            variables.append("startDate", new Date(this.state.startDate).getTime())
        } else {
            variables.append("startDate", '')
        }
        if (this.state.endDate != '') {
            variables.append("endDate", new Date(this.state.endDate).getTime())
        } else {
            variables.append("endDate", '')
        }

        variables.append("billing", this.state.projectBillingModalText)
        variables.append("currency", this.state.projectCurrencyModalText == "Currency" ? "" : this.state.projectCurrencyModalText)
        getLog("=====" + this.state.userToken)
        return webservice(variables, "project/update", 'POST', this.state.userToken)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {

                    arr[this.state.editedIndex].name = this.state.projectNameModalText;
                    arr[this.state.editedIndex].id = this.state.projectCodeModalText;
                    arr[this.state.editedIndex].costPerHour = "" + this.state.projectBillingModalText;
                    arr[this.state.editedIndex].currency = this.state.projectCurrencyModalText;
                    arr[this.state.editedIndex].validityFrom = this.state.startDate;
                    arr[this.state.editedIndex].validityTo = this.state.endDate;

                    this.setState({ projectList: arr })
                    setTimeout(() => {
                        this.setState({ projectModalVisible: false })
                        this.setState({
                            projectNameModalText: '',
                            projectCodeModalText: '',
                            spinnerVisible: false,
                            projectBillingModalText: '',
                            projectCurrencyModalText: '',
                            startDate: '',
                            endDate: '',
                            projectNameMandatory: false,
                            projectNameMandatoryText: ''
                        })
                    }, 300)
                }

            })
    }
    addProjectAPI() {
        var arr = this.state.projectList
        let variables = new FormData();
        this.setState({ spinnerVisible: true, projectNameMandatory: false, projectNameMandatoryText: '' })
        variables.append("projectName", this.state.projectNameModalText)
        variables.append("projectCode", this.state.projectCodeModalText)
        if (this.state.startDate != '') {
            variables.append("startDate", new Date(this.state.startDate).getTime())
        } else {
            variables.append("startDate", '')
        }
        if (this.state.endDate != '') {
            variables.append("endDate", new Date(this.state.endDate).getTime())
        } else {
            variables.append("endDate", '')
        }

        variables.append("billing", this.state.projectBillingModalText)
        variables.append("currency", this.state.projectCurrencyModalText == "Currency" ? "" : this.state.projectCurrencyModalText)
        getLog("=====" + this.state.userToken)
        return webservice(variables, "project/add", 'POST', this.state.userToken)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {

                    arr.push(
                        {
                            name: this.state.projectNameModalText,
                            id: this.state.projectCodeModalText,
                            costPerHour: "" + this.state.projectBillingModalText,
                            currency: this.state.projectCurrencyModalText,
                            validityFrom: this.state.startDate,
                            validityTo: this.state.endDate,
                            projectId: response.projectData.projectId
                        }
                    )
                    this.setState({ projectList: arr })
                    setTimeout(() => {
                        this.setState({ projectModalVisible: false })



                        this.setState({
                            projectNameModalText: '',
                            projectCodeModalText: '',
                            spinnerVisible: false,
                            projectBillingModalText: '',
                            projectCurrencyModalText: '',
                            startDate: '',
                            endDate: '',
                            projectNameMandatory: false,
                            projectNameMandatoryText: ''
                        })

                    }, 300)
                }

            })
    }
    projectModalSaveButtonClicked() {
        this.setState({ minDate: '' })
        var arr = this.state.projectList

        if (this.state.projectModalTitleText == "Edit Project") {

            if (this.state.projectNameModalText != '') {

                if (this.state.projectBillingModalText == '' && this.state.projectCurrencyModalText == "Currency") {
                    this.editProjectAPI()
                } else if (this.state.projectBillingModalText != '' && this.state.projectCurrencyModalText != "Currency") {
                    this.editProjectAPI()
                } else {
                    //alert(this.state.projectCurrencyModalText)
                    this.state.projectBillingModalText == '' ?
                        this.setState({ projectNameMandatory: true, projectNameMandatoryText: "*Please enter Billing rate in per hour." })
                        :
                        this.setState({ projectNameMandatory: true, projectNameMandatoryText: "*Please enter currency." })


                }

                //alert(this.state.projectId)


            } else {
                this.setState({ projectNameMandatory: true, projectNameMandatoryText: "*Please enter project name" })
            }

        } else {
            if (this.state.projectNameModalText != '') {

                if (this.state.projectBillingModalText == '' && this.state.projectCurrencyModalText == "Currency") {
                    this.addProjectAPI()
                } else if (this.state.projectBillingModalText != '' && this.state.projectCurrencyModalText != "Currency") {
                    this.addProjectAPI()
                } else {
                    this.state.projectBillingModalText == '' ?
                        this.setState({ projectNameMandatory: true, projectNameMandatoryText: "*Please enter Billing rate in per hour." })
                        :
                        this.setState({ projectNameMandatory: true, projectNameMandatoryText: "*Please enter currency." })


                }


            } else {
                this.setState({ projectNameMandatory: true, projectNameMandatoryText: "*Please enter project name" })
            }
        }

        // this.setState({ projectList: arr })
        // setTimeout(() => {
        //     this.setState({ projectModalVisible: false })
        //     this.setState({
        //         projectNameModalText: '',
        //         projectCodeModalText: '',
        //         spinnerVisible: false,
        //         projectBillingModalText: '',
        //         projectCurrencyModalText: '',
        //         startDate: '',
        //         endDate: ''
        //     })
        // }, 300)
    }
    hitAddLeaveTypeAPI() {
        var arr = this.state.leaveTypeList
        let variables = new FormData();
        //this.setState({ spinnerVisible: true, projectNameMandatory: false })
        variables.append("leaveTypeName", this.state.leaveTypeTextInputDataOnModal)
        variables.append("balance", this.state.leaveBalance)
        variables.append("startDate", new Date(this.state.startDate).getTime())
        variables.append("endDate", new Date(this.state.endDate).getTime())


        return webservice(variables, "leavetype/add", 'POST', this.state.userToken)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {
                    arr.push(
                        {
                            name: this.state.leaveTypeTextInputDataOnModal,
                            leaveCount: this.state.leaveBalance,
                            validityFrom: this.state.startDate,
                            validityTo: this.state.endDate,
                            leaveTypeId: response.leaveTypeData.leaveTypeId

                        }
                    )

                    this.setState({ leaveTypeList: arr })
                    getLog("Leave type add response=====++++" + JSON.stringify(arr))
                    setTimeout(() => {
                        this.setState({ leaveModalVisible: false })
                        this.setState({
                            leaveTypeTextInputDataOnModal: '',
                            leaveBalance: '',
                            startDate: '',
                            endDate: ''
                        })
                    }, 300)
                } else {
                    getLog("Leave type add response=====++++" + JSON.stringify(response))

                }

            })
    }
    hitEditLeaveTypeAPI() {
        var arr = this.state.leaveTypeList
        let variables = new FormData();
        //this.setState({ spinnerVisible: true, projectNameMandatory: false })
        variables.append("leaveTypeName", this.state.leaveTypeTextInputDataOnModal)
        variables.append("balance", this.state.leaveBalance)
        variables.append("startDate", new Date(this.state.startDate).getTime())
        variables.append("endDate", new Date(this.state.endDate).getTime())
        variables.append("leaveTypeId", this.state.leaveTypeId)


        return webservice(variables, "leavetype/edit", 'POST', this.state.userToken)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {
                    arr[this.state.editedIndex].name = this.state.leaveTypeTextInputDataOnModal;
                    arr[this.state.editedIndex].leaveCount = this.state.leaveBalance;
                    arr[this.state.editedIndex].validityFrom = this.state.startDate;
                    arr[this.state.editedIndex].validityTo = this.state.endDate;


                    this.setState({ leaveTypeList: arr })
                    getLog("Leave type edit response=====++++" + JSON.stringify(arr))
                    setTimeout(() => {
                        this.setState({ leaveModalVisible: false })
                        this.setState({
                            leaveTypeTextInputDataOnModal: '',
                            leaveBalance: '',
                            startDate: '',
                            endDate: ''
                        })
                    }, 300)
                } else {
                    getLog("Leave type edit response=====++++" + JSON.stringify(response))

                }

            })
    }
    leaveModalSaveButtonClicked() {
        if (this.state.leaveTypeTextInputDataOnModal == '') {
            this.setState({ leaveModalErrorText: "*Please enter leave type" })
            return
        } else if (this.state.leaveBalance == '') {
            this.setState({ leaveModalErrorText: "*Please enter leave balance" })
            return
        } else if (this.state.startDate == '') {
            this.setState({ leaveModalErrorText: "*Please enter start date" })
            return
        } else if (this.state.endDate == '') {
            this.setState({ leaveModalErrorText: "*Please enter end date" })
            return
        } else {


            if (this.state.leaveModalTitleText == "Edit Leave Type") {
                this.hitEditLeaveTypeAPI()


            } else {
                this.hitAddLeaveTypeAPI()

            }

            //this.setState({ leaveTypeList: arr })

        }



    }
    render() {
        return (
            <SafeAreaView
                style={styles.safeArea}>
                <View style={styles.container}>
                    <Header HeaderLeftText="Settings"
                        toggleDrawer={() => this.props.navigation.openDrawer()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIcon={require('../images/settingIcons/filterIcon.png')}
                        headerRightIconOnPress={() => this.filterFunction()} />
                    <Spinner visible={this.state.spinnerVisible} />
                    <ProjectModal
                        modalVisible={this.state.projectModalVisible}
                        modalClose={() => this.closeProjectModal()}
                        TitleText={this.state.projectModalTitleText}
                        ProjectNameValue={this.state.projectNameModalText}
                        projectNameChangeText={(text) => this.setState({ projectNameModalText: text, projectNameMandatory: false, projectNameMandatoryText: '' })}
                        ProjectCodeValue={this.state.projectCodeModalText}
                        projectCodeChangeText={(text) => this.setState({ projectCodeModalText: text })}
                        BillingValue={this.state.projectBillingModalText}
                        billingTextInputData={(text) => this.setState({ projectBillingModalText: text })}
                        Currency={this.state.projectCurrencyModalText}

                        saveButtonClicked={() => this.projectModalSaveButtonClicked()}
                        datePickerVisible={this.state.projectModalDatePickerVisible}
                        startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
                        datePickerCloseFunction={() => this.setState({ projectModalDatePickerVisible: false })}
                        endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
                        dateSelected={(date) => this.getDateFromModal(date)}
                        startDate={this.state.startDateToShowOnMOdal}
                        endDate={this.state.endDateToShowOnModal}
                        minDate={this.state.minDate == '' ? new Date('01/01/1990') : this.state.minDate}
                        CurrencyOptionArrayOnProjectModal={["None", "$", "€", "£", "₹"]}
                        selectCurrencyClicked={(index, value) => this.setState({ projectCurrencyModalText: value == "None" ? "Currency" : value })}
                        projectNameMandatory={this.state.projectNameMandatory}
                        projectNameMandatoryText={this.state.projectNameMandatoryText}
                    />





                    <LeaveModal
                        modalVisible={this.state.leaveModalVisible}
                        datePickerVisible={this.state.projectModalDatePickerVisible}
                        datePickerVisibleFunction={() => this.setState({ projectModalDatePickerVisible: true })}
                        startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
                        endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
                        dateSelected={(date) => this.getDateFromModal(date)}
                        startDate={this.state.startDateToShowOnMOdal}
                        endDate={this.state.endDateToShowOnModal}
                        totalDays={this.state.totalDays}
                        datePickerCloseFunction={() => this.setState({ projectModalDatePickerVisible: false })}
                        minDate={this.state.minDate == '' ? new Date('01/01/1990') : this.state.minDate}
                        saveButtonClicked={() => this.leaveModalSaveButtonClicked()}
                        reasonTextInputData={(text) => this.setState({ reasonTextInputData: text })}
                        modalClose={() => this.closeLeaveModal()}
                        TitleText={this.state.leaveModalTitleText}
                        TypePlaceHolder={this.state.leaveTypeOnLeaveModal}
                        leavesOptionArrayOnLeaveModal={this.state.leaveTypeListPassOnModal}
                        selectLeaveTypeClicked={(index, value) => this.onLeaveModalselectLeaveTypeClicked(index, value)}
                        balanceTextInputData={this.state.leaveBalance}
                        balanceTextInputChange={(text) => this.setState({ leaveBalance: text })}
                        leaveTypeTextInputData={this.state.leaveTypeTextInputDataOnModal}
                        leaveTypeTextInputChange={(text) => this.setState({ leaveTypeTextInputDataOnModal: text })}
                        errorText={this.state.leaveModalErrorText}
                    />
                    {this.state.flatListdata == this.state.activitiList ? null :
                        <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />

                    }

                    <View style={styles.mainContainer}>
                        {this.makeTabs()}
                        <ScrollView
                            {...this.wrapperPanResponder.panHandlers}
                        >
                            <FlatList
                                data={this.state.flatListdata}
                                //data={this.state.tabId == "Projects" ? this.state.projectList : this.state.tabId == "Leave Types" ? this.state.leaveTypeList : this.state.activitiList}
                                renderItem={({ item, index }) => this.createList(item, index)}
                                keyExtractor={key => key.index}
                                extraData={this.state}
                            // onEndReachedThreshold={10}
                            // onEndReached={({ distanceFromEnd }) => {
                            //     alert('on end reached ', distanceFromEnd)
                            // }}
                            />
                            {
                                this.state.tabId == "Activities" ? null : <View style={{ height: 80 }} />
                            }

                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    container: { flex: 1, backgroundColor: '#fff', },
    mainContainer: { flex: 1 },
    tabView: { height: 40, width: width, flexDirection: 'row', backgroundColor: "#2A56C6" },
    tabStyle: { flex: 1, alignItems: "center", justifyContent: "center" },
    weekTabView: { height: 20, width: width, flexDirection: 'row', backgroundColor: "#979797" },
    tabText: { fontSize: fontSizes.font14, color: "white", textAlign: 'center', fontWeight: "bold" },
    listRowView: { flex: 1, flexDirection: "row", padding: 10, borderBottomColor: "#E5E5E5", borderBottomWidth: 1, },
    projectNameText: { fontSize: fontSizes.font14, color: "#333333", textAlign: 'left', margin: 2 },
    otherText: { fontSize: fontSizes.font12, color: "#979797", textAlign: 'left', margin: 2 },


});
