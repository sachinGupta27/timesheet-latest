export default function graphQLRequest(variables, type, apiMethod, token) {
    const siteUrl = "http://timesheet.greychaindesign.com/api/v1/";
    var init = apiMethod == "GET" ? {
        method: "GET",
        headers: {
            'Content-Type': "multipart/form-data",
            'token': token ? token : ""
        },
    } :
        {
            method: apiMethod,
            headers: {

                'Content-Type': "multipart/form-data",
                'token': token ? token : "",
            },
            body: variables
        }

    console.log(siteUrl + type + "        init" + JSON.stringify(init));

    return fetch(siteUrl + type, init)
        .then(res => res.json().then(data => {

            if (data.code == 200 || data.code == 201) {
                console.log("data on webservice file" + JSON.stringify(data))
                return data;
            }
            else if (data.code == 400) {
                setTimeout(() => { alert(data.message); }, 500)
                return "error"
            }
            else if (data.code == 401) {
                setTimeout(() => { alert(data.message); }, 500)
                return "error"
            }

        }))

        .catch(err => {
            console.log("err" + JSON.stringify(err))
            setTimeout(() => { alert("Something went wrong. Please try again."); }, 500)
            
            return "error"

        });
};