import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View, Image, TouchableOpacity, Platform, StatusBar
} from 'react-native';
import { height, width, fontSizes, dynamicSize } from '../utils/utils'
//Here all the functions defination are defined at every particular screen that what the function does
//Text and icons are passed from every particular screen
//Here HeaderLeftText is used for the text that just next to the menu icon
//toggleDrawer is the function that toggles drawer and someTimes it is also used for navigation goBack to previous screen.
//type is just used ti identify that header contain date or not because date is too long thats wht the width or flex of view is managed according to it using conditional operator
//HeaderRightText is text places right to header
//headerLeftIcon & headerRightIcon is the icon of left or right to header
//headerLeftIconOnPress & headerRightIconOnPress is the function that defined what happen on presssing that icons
//headerType defines the type of header it is of two type one is menu drawer that opens the drawer and its icon is menu icon and other one is back icon that go back to previous page.
//save is true when there is save button in the header so the UI of right view is changed to first tick icon and then save text
const Header = ({ HeaderLeftText, toggleDrawer, type, HeaderRightText, headerLeftIcon, headerLeftIconOnPress, headerRightIcon, headerRightIconOnPress, headerType, save }) => {

    return (
        <View>
            {/* {Platform.OS == 'ios' ? <View style={styles.iOSTopBar}>
            </View> : <StatusBar
                    backgroundColor="#21459E"
                    barStyle="light-content"
                />} */}
            <StatusBar
                translucent
                backgroundColor="#21459E"
                barStyle="light-content"
            />
            <View style={styles.headerContainer}>
                <TouchableOpacity onPress={toggleDrawer}
                    style={styles.menuView}>
                    <Image source={headerType == 'back' ? require('../images/headerIcons/leftArrow.png') : require('../images/headerIcons/menu.png')} />
                </TouchableOpacity>
                <View style={styles.headerDataContainer}>
                    <View style={[styles.leftView, { flex: type == "leaveDescription" ? 8.5 : 5 }]}>
                        <Text numberOfLines={1} style={[styles.headerText, { fontSize: type == "leaveDescription" ? fontSizes.font16 : fontSizes.font18 }]}>
                            {HeaderLeftText}
                        </Text>
                        <TouchableOpacity style={{ padding: 10 }} onPress={headerLeftIconOnPress}>
                            <Image source={headerLeftIcon} style={{ marginTop: 2 }} />
                        </TouchableOpacity>
                    </View>
                    {/* this condition specify that there is no rightText and Icon so its cover all the flex */}
                    {headerRightIcon != '' && HeaderRightText != '' ?
                        save ?
                            <TouchableOpacity onPress={headerRightIconOnPress}
                                style={[styles.rightView, { flex: type == 'date' ? 3.5 : 2.5 }]}>

                                <View >
                                    <Image source={headerRightIcon} style={{ marginTop: 2 }} />
                                </View>
                                <Text style={[styles.headerRightText, { fontSize: type == 'date' ? fontSizes.font10 : fontSizes.font14, marginLeft: 5 }]}>
                                    {HeaderRightText}
                                </Text>
                            </TouchableOpacity>
                            :
                            <View style={[styles.rightView, { flex: type == 'date' ? 8.5 : 2.5 }]}>
                                <Text style={[styles.headerRightText, { fontSize: type == 'date' ? fontSizes.font13 : fontSizes.font14 }]}>
                                    {HeaderRightText}
                                </Text>
                                <TouchableOpacity style={{ padding: 10, paddingLeft: 0 }} onPress={headerRightIconOnPress}>
                                    <Image source={headerRightIcon} style={{ marginTop: 2 }} />
                                </TouchableOpacity>
                            </View>
                        // save specify that if it true then there is tick icon after that save text otherwise first text then icon

                        : null}

                </View>
            </View>
        </View>
    )
}




const styles = StyleSheet.create({
    headerContainer: {
        height: 60,
        backgroundColor: '#2A56C6',
        flexDirection: "row",
        marginTop: Platform.OS == 'ios' ? 0 : 20
    },
    iOSTopBar: {
        backgroundColor: '#21459E',
    },
    menuView: { height: 55, width: 40, alignItems: "center", justifyContent: "center", marginTop: 2 },
    headerDataContainer: { flex: 1, marginRight: 10, flexDirection: "row" },
    leftView: { flex: 5, flexDirection: "row", alignItems: "center" },
    rightView: { flex: 2.5, flexDirection: "row", alignItems: "center", justifyContent: "flex-end" },
    headerText: { color: "white", fontWeight: "bold", marginRight: 5 },
    headerRightText: { color: "white", fontSize: fontSizes.font16, fontWeight: "bold", marginRight: 5 }


});

export default Header;