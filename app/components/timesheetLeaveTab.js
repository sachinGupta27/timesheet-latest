import React, { Component } from 'react';
import {
    StyleSheet,
    Text, TouchableWithoutFeedback,
    View, Image, TouchableOpacity, Platform, StatusBar
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'


const TimeLeaveTab = ({ onTabPress, tabPress }) => {
    return (
        <View style={styles.tabView}>
            {/* first tab */}
            <TouchableWithoutFeedback onPress={onTabPress} >
                <View
                    style={[styles.tabStyle, { borderBottomColor: "#ffffff", borderBottomWidth: tabPress ? 2 : 0 }]} >
                    <Text style={styles.tabText}>
                        Timesheet
                    </Text>
                </View>
            </TouchableWithoutFeedback>
            {/* second tab */}
            <TouchableWithoutFeedback onPress={onTabPress} >
                <View
                    style={[styles.tabStyle, { borderBottomColor: "#ffffff", borderBottomWidth: tabPress ? 0 : 2 }]}   >
                    <Text style={styles.tabText}>
                        Leaves
                    </Text>
                </View>
            </TouchableWithoutFeedback>
            {/* third tab */}
            {/* <View style={styles.tabStyle} >
            </View> */}
        </View>
    )
}




const styles = StyleSheet.create({
    tabView: { height: 40, width: width, flexDirection: 'row', backgroundColor: "#2A56C6" },
    tabStyle: { flex: 1, alignItems: "center", justifyContent: "center" },
    weekTabView: { height: 20, width: width, flexDirection: 'row', backgroundColor: "#979797" },
    tabText: { fontSize: fontSizes.font16, color: "white", textAlign: 'center', fontWeight: "bold" },



});

export default TimeLeaveTab;