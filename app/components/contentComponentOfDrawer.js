




/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform, TouchableOpacity,
    StyleSheet, AsyncStorage, SafeAreaView,
    Text, ScrollView, Alert, TextInput,
    View, Image, TouchableWithoutFeedback
} from 'react-native';
import { height, width, fontSizes, getLog } from '../utils/utils'
import { DrawerNavigator, DrawerItems, StackNavigator } from 'react-navigation';
import { StackActions, NavigationActions } from 'react-navigation';
import webservice from '../components/webService';
import Setting from '../screens/appSetting';
// const graphData=[
//     {}
// ]

export default class contentComponent extends Component {
    constructor(props) {

        super(props)
        this.state = {
            data: {},
            email: '',
            edit: false,
            //returnCallBack: { value: false, callBack: () => { } },
            userImage: require('../images/userImage.png'), type: 'user'
        }
    }
    componentWillMount() {

        AsyncStorage.getItem('LoginresponseData').then((response) => {
            this.setState({ data: JSON.parse(response) })
            setTimeout(() => {
                this.setState({ email: this.state.data.email })
            })

        })
        //this.setState({ returnCallBack: { value: false, callBack: this.callBackCalledOnChangeImageAndEmailOnProfileScreen } })
    }
    // callBackCalledOnChangeImageAndEmailOnProfileScreen = (type, data) => {
    //     if (type == 'image') {
    //         this.setState({ userImage: data })
    //     } else {
    //         this.setState({ email: data })
    //     }
    // }
    componentDidMount() {
        Setting.settingCallback(this.callBack)
    }

    callBack = (data) => {
        alert(data)
        this.setState({email:data})
        //alert(data)
    }

    logout() {
        AsyncStorage.getItem('userToken').then((token) => {

            return webservice('', "logout", 'GET', token)
                .then(response => {
                    //this.setState({spinnerVisible:false})
                    if (response != "error") {
                        AsyncStorage.removeItem('userToken').then((token) => {
                            getLog("tokenRemoved")
                        })

                        AsyncStorage.removeItem('LoginresponseData').then((token) => {
                            getLog("LoginresponseDataRemoved")
                        })
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'Login' })],
                        });
                        this.props.item.navigation.dispatch(resetAction);

                    }

                })
        })

    }
    logoutPress() {
        //navigation.navigate('Timesheet')
        Alert.alert(
            'Logout',
            'Are you sure you want to logout?',
            [
                { text: 'Cancel', onPress: () => getLog('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => this.logout() },
            ],
            { cancelable: false }
        )
    }
    edit() {
        this.props.item.navigation.navigate('EditProfile', { 'addItem': (item) => this.props.item.navigation.closeDrawer() });
        //this.props.item.navigation.closeDrawer()
    }
    onChangeEmail(text) {
        this.setState({ email: text })
    }
    render() {

        return (
            <SafeAreaView
                style={styles.safeArea}>
                <View style={styles.container}>
                    <ScrollView style={{ marginBottom: 10, marginTop: Platform.OS == 'ios' ? 0 : 20 }}>
                        <View style={styles.topView}>
                            <View style={styles.userDataView}>
                                <Image source={this.state.userImage}
                                    style={styles.userImage} />
                                <View style={[styles.emailView, {}]}>
                                    <Text style={styles.emailText}>
                                        {this.state.email}

                                    </Text>
                                    {/* <TextInput
                                        style={{ color: "#fff" }}
                                        multiline={true}
                                        placeholder="abc@gmail.com"
                                        placeholderTextColor="#fff"
                                        underlineColorAndroid="transparent"
                                        editable={this.state.edit}
                                        onChangeText={(text) => this.onChangeEmail(text)}
                                        value={this.state.email}
                                    //keyboardType="numeric"
                                    /> */}
                                    {/* <TouchableOpacity style={{ padding: 10, }} onPress={() => this.edit()}>
                                        <Image source={require('../images/drawerIcons/editIcon.png')} />
                                    </TouchableOpacity> */}
                                </View>
                            </View>
                        </View>
                        <DrawerItems
                            {...this.props.item}
                            labelStyle={{ fontSize: fontSizes.font16 }}
                            activeTintColor='#2A56C6'



                        />
                        {/* {this.state.type != 'user' ? */}
                            {/* <TouchableWithoutFeedback
                                onPress={() => this.props.item.navigation.navigate('Setting', { props: this.props.item.navigation })}
                            >
                                <View style={{ flexDirection: "row", paddingLeft: 20, padding: 8 }}>
                                    <Image source={require('../images/drawerIcons/logoutIcon.png')} />
                                    <Text style={{ fontSize: fontSizes.font16, color: '#333333', marginLeft: 38, fontWeight: "bold" }}>
                                        Setting
                                    </Text>
                                </View>


                            </TouchableWithoutFeedback> */}
                            {/* : null} */}

                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    topView: {
        height: 120,
        width: width - (width / 5),
        backgroundColor: "#2A56C6",
        alignItems: "center",
        justifyContent: 'center'
    },
    userDataView: {
        width: (width - (width / 5)) - 40,

        //paddingRight: Platform.OS == 'ios' ? 0 : 30
    },
    userImage: { height: 60, width: 60, borderRadius: 30, marginTop: 20, },
    emailText: {
        textAlign: 'left',
        color: '#fff',

    },
    emailView: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 10, }

});





