import React from 'react';
import { Text, StyleSheet, ActivityIndicator, Modal, Dimensions, View, Image } from 'react-native';

const { height, width } = Dimensions.get('window');

const Spinner = ({visible}) => {
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={visible}
            onRequestClose={() => {
                console.log('Modal has been closed.');
            }}>
            <View style={{ height: height, width: width, alignItems: "center", justifyContent: "center", backgroundColor: "#00000092" }}>
                <ActivityIndicator size="large" color="#2A56C6" />
               
            </View>

        </Modal>
    )
}



export default Spinner;