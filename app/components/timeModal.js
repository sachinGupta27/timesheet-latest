
import React, { Component } from 'react';
import {
    StyleSheet,
    Text, Modal, Picker, TouchableWithoutFeedback,
    View, Image, TouchableOpacity, TextInput, ScrollView, Platform
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'

const TimeModal = ({ modalVisible, modalClose, hourUpBTn, hourDownBTn, minDownBTn, minUpBTn, onConfirmPress, hourText, minuteText }) => {

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={modalClose}>
            <View style={styles.modalContainer}>
                <TouchableOpacity onPress={modalClose} style={{ flex: 1, width: width }} />
                <View style={styles.centerContainer}>
                    <Text style={styles.overrideText}>
                        Override Time
                    </Text>
                    <Text style={{ fontSize: fontSizes.font14, marginBottom: 10, color: "#979797" }}>
                        Select duration
                    </Text>
                    <View style={styles.selectDuration}>
                        <View style={{flex:1}}>
                            <TouchableOpacity onPress={hourUpBTn}
                                style={styles.arrowView}>
                                <Image source={require('../images/createTimesheet/arrowTop.png')} />
                            </TouchableOpacity>
                            <View style={{ flexDirection: "row",alignSelf:"center" }}>
                                <Text style={styles.numericText}>
                                    {hourText}
                                </Text>
                                <Text style={styles.alphaText}>
                                    h
                                </Text>
                            </View>
                            <TouchableOpacity onPress={hourDownBTn}
                                style={styles.arrowView}>
                                <Image source={require('../images/createTimesheet/arrowDown.png')} />
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:1}}>
                            <TouchableOpacity onPress={minUpBTn}
                                style={styles.arrowView}>
                                <Image source={require('../images/createTimesheet/arrowTop.png')} />
                            </TouchableOpacity>
                            <View style={{ flexDirection: "row",alignSelf:"center" }}>
                                <Text style={styles.numericText}>
                                {minuteText}
                                </Text>
                                <Text style={styles.alphaText}>
                                    m
                                </Text>
                            </View>
                            <TouchableOpacity onPress={minDownBTn}
                                style={styles.arrowView}>
                                <Image source={require('../images/createTimesheet/arrowDown.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity style={{ padding: 10, paddingTop: 0 }} onPress={onConfirmPress}>
                        <Text style={styles.confirmText}>
                            Confirm
                    </Text>
                    </TouchableOpacity>

                </View>
                <TouchableOpacity onPress={modalClose} style={{ flex: 1, width: width }} />
            </View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    modalContainer: { height: height, width: width, backgroundColor: "#00000090", alignItems: "center", justifyContent: "center" },
    centerContainer: { width: "80%", padding: 15, backgroundColor: "white", paddingRight: Platform.OS == 'ios' ? 15 : 25 },
    arrowView: { padding: 10, alignItems: "center", justifyContent: "center",marginVertical:10 },
    numericText: { fontSize: 32, color: "#2A56C6", fontWeight: "bold" },
    alphaText: { fontSize: 32, color: "#979797", fontWeight: "bold" },
    confirmText: { fontSize: fontSizes.font14, marginBottom: 10, color: "#2A56C6", alignSelf: "flex-end" },
    overrideText: { fontSize: fontSizes.font18, marginBottom: 10, color: "#333333", fontWeight: 'bold' },
    selectDuration: { flexDirection: "row", justifyContent: "space-between", marginHorizontal: 40, marginVertical: 40 }

});






export default TimeModal;